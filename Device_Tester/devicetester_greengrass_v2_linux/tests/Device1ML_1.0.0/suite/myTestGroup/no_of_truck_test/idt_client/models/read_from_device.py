# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model, BytesConverter


class ReadFromDeviceRequest(Model):
    """
    A read from device request.

    :ivar str command: The command to issue. Must be one of Open, Read, Close.
    Command info:
       - Open causes the underlying connection to be opened. This must be issued before Read.
       - Read reads data from the underlying connection and should be called only after Open. Read can be called many times to keep reading data.
       - Close causes the underlying connection to be closed. No more Reads should be issued before issuing another Open.
     IDT will call Close at the end of each test case if the connection is not already closed.
    :ivar str device_name: The name of the device to read from. Leave None to use the device under test - otherwise use the name specified in the test.json for resource devices.
    """
    OPEN_COMMAND = "Open"
    READ_COMMAND = "Read"
    CLOSE_COMMAND = "Close"

    _attributes = ['command', 'device_name']

    def __init__(self, command, device_name=None):
        self.command = command
        self.device_name = device_name


class ReadFromDeviceResponse(Model):
    """
    The response to a read from device request.

    :ivar bytes read_message: The bytes that were read. May be empty if no data was read.
    :ivar bool eof: Whether an end of file indicator was encountered when reading from the device.
    """
    _attributes = [('bytes', BytesConverter), 'eof']

    def __init__(self, bytes, eof):
        self.bytes = bytes
        self.eof = eof
