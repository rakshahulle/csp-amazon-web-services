from idt_client import *

def main():
    # Use the client SDK to communicate with IDT
    client = Client()
    
    # Create an execute on device request
    exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("python3 purinferpubsub.py --endpoint <IoT_ENDPOINT> --ca_file <PATH_TO_ROOT_CA_FILE> --cert <PATH_TO_THING_CERTIFICATE_FILE> --key <PATH_TO_PRIVATE_KEY> --client_id dev_tester --topic <DETECTION_STATISTICS_PUBLISH_TOPIC> --count 0"))
    #print(exec_req)

   # exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("docker -v ' docker present'"))
    #exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("sc query greengrass''greengrass is running '"))
   # exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("sudo systemctl status greengrass.service' 'gg is present'"))
    
    
    exec_resp = client.execute_on_device(exec_req)
    print("--------------------------------------------------------")
    # Print the standard output
    print(exec_resp.stdout.decode("utf-8") )
    print("--------------------------------------------------------")
    # Create a send result request
    sr_req = SendResultRequest(TestResult(passed=True))
     
    # Send the result
    client.send_result(sr_req)
    
    
    
    
   
    
    
   
    
    
    #echo
    #chmod
    #cp
    #grep
    #kill
    #ln
    #mkinfo
    #uname
    #printf
    
       
if __name__ == "__main__":
    main()

