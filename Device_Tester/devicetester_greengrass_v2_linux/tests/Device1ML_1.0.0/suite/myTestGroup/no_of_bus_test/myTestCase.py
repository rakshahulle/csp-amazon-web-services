from idt_client import *

import boto3
import time


def main():
    # Use the client SDK to communicate with IDT
    client = Client()
    
    logs = boto3.client('logs', region_name='<AWS_REGION>',
                            aws_access_key_id='<AWS_ACCESS_KEY>',
                            aws_secret_access_key='<AWS_SECRET_KEY>') 

    try:
        LOG_GROUP='DEVICE'
        LOG_STREAM='ML_TestCases'
        
        logs.create_log_group(logGroupName=LOG_GROUP)
        logs.create_log_stream(logGroupName=LOG_GROUP, logStreamName=LOG_STREAM)
    except:
        print("Log group already exists.....Using the same.....\n")

    timestamp = int(round(time.time() * 1000))
   
    
    #exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("aws --version ' '"))
    exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("python3 bus_detection.py --endpoint <IoT_ENDPOINT> --ca_file <PATH_TO_ROOT_CA_FILE> --cert <PATH_TO_THING_CERTIFICATE_FILE> --key <PATH_TO_PRIVATE_KEY> --client_id dev_tester --topic <DETECTION_STATISTICS_PUBLISH_TOPIC> --count 0"))
    
    # Run the command
    exec_resp = client.execute_on_device(exec_req)
    
    # Print the standard output
    print(exec_resp.stdout.decode("utf-8"))

    p= exec_resp.stdout.decode("utf-8")
    

     #Create a send result requestss
    sr_req = SendResultRequest(TestResult(passed=True))
    # Send the result
    client.send_result(sr_req)

    
    if "Total Number of detected bus" in p:
        sr_req = SendResultRequest(TestResult(passed=True))
        # print("passesssss")

        response = logs.put_log_events(
            logGroupName=LOG_GROUP,
            logStreamName=LOG_STREAM,
            logEvents=[

                {
                        'timestamp': timestamp,
            
                        'message':'{"TestCase":"test_no_of_bus_detected", "Result":"passed"}'
                }
            ]       

            
        )
    else:
        sr_req = SendResultRequest(TestResult(passed=False))
        print("failled")
        response = logs.put_log_events(
            logGroupName=LOG_GROUP,
            logStreamName=LOG_STREAM,
            logEvents=[
                    {
                        'timestamp': timestamp,
            
                        'message':'{"TestCase":"test_no_of_bus_detected", "Result":"failed"}'
                    }
            ]
        
        )



'''''
def main():
    # Use the client SDK to communicate with IDT
    client = Client()
    
    # Create an execute on device request
    exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("python3 bus_detection.py --endpoint a1frq7egwcqa3o-ats.iot.ap-south-1.amazonaws.com --ca_file rootCA.pem --cert thingCert.crt --key privKey.key --client_id basicPubSub --topic rk3399/stats --count 0' '"))
    #print(exec_req)

   # exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("docker -v ' docker present'"))
    #exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("sc query greengrass''greengrass is running '"))
   # exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("sudo systemctl status greengrass.service' 'gg is present'"))
    
    exec_resp = client.execute_on_device(exec_req)
    print("--------------------------------------------------------")
    # Print the standard output
    print(exec_resp.stdout.decode("utf-8") )
    print("--------------------------------------------------------")
    # Create a send result request
    sr_req = SendResultRequest(TestResult(passed=True))
     
    # Send the result
    client.send_result(sr_req)
    
    
   
    
    
    #echo
    #chmod
    #cp
    #grep
    #kill
    #ln
    #mkinfo
    #uname
    #printf
    
'''



if __name__ == "__main__":
    main()

