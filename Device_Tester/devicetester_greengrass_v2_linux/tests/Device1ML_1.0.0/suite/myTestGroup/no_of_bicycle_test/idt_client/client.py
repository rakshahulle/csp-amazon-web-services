# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

import json

from urllib3.poolmanager import PoolManager
import os

from . import GetContextValueRequest, GetContextStringResponse
from .exceptions import Non2xxResponseException, ExecuteOnHostCommandFailedException, \
    ExecuteOnHostCommandTimedOutException, ExecuteOnDeviceCommandFailedException, \
    ExecuteOnDeviceCommandTimedOutException, GetContextValueUnresolvedJsonPathsException
from .models.utils import ModelConverter
from .internal import ErrorMessageResponseWrapper, SendResultResponseWrapper, CopyToDeviceResponseWrapper, \
    ReadFromDeviceResponseWrapper, PollForNotificationsResponseWrapper, GetContextValueResponseWrapper, \
    ExecuteOnHostResponseWrapper, ExecuteOnDeviceResponseWrapper


class Client:
    """
    The client that interacts with the IDT server. Sends requests and receives responses.
    """

    def __init__(self):
        self._url = f"http://localhost:{self._fetch_env_var('IDT_SERVER_PORT')}"
        self._secret = self._fetch_env_var('IDT_SECRET', False)
        self._unique_id = self._fetch_env_var('IDT_UNIQUE_ID', False)
        self._pool = PoolManager()

    def execute_on_host(self, request):
        """
        Execute commands on the local machine.

        :param request: An :class:`.ExecuteOnHostRequest`.
        :return: An :class:`.ExecuteOnHostResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        :raises ExecuteOnHostCommandFailedException: If a command executed exits with a non-zero exit code.
        :raises ExecuteOnHostCommandTimedOutException: If a command executed times out.
        """
        response_wrapper = self._make_request(request, 'executeOnHost/v1', ExecuteOnHostResponseWrapper)

        if response_wrapper.data.timed_out:
            raise ExecuteOnHostCommandTimedOutException(response_wrapper.data, response_wrapper.error_message)

        if response_wrapper.data.exit_code != 0:
            raise ExecuteOnHostCommandFailedException(response_wrapper.data, response_wrapper.error_message)

        return response_wrapper.data

    def copy_to_device(self, request):
        """
        Copy a file or directory from the computer running IDT to a device. The device connection type must be SSH to copy files or directories to it.

	    The cat command must be available on the device to copy files. The mkdir and cat commands must be available to copy directories.

        :param request: A :class:`.CopyToDeviceRequest`.
        :return: A :class:`.CopyToDeviceResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        """
        return self._make_request(request, 'copyToDevice/v1', CopyToDeviceResponseWrapper).data

    def execute_on_device(self, request):
        """
        Execute commands on a device. The device connection type must be SSH to execute commands on it.

        :param request: An :class:`.ExecuteOnDeviceRequest`.
        :return: An :class:`.ExecuteOnDeviceResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        :raises ExecuteOnDeviceCommandFailedException: If a command executed exits with a non-zero exit code.
        :raises ExecuteOnDeviceCommandTimedOutException: If a command executed times out.
        """
        response_wrapper = self._make_request(request, 'executeOnDevice/v1', ExecuteOnDeviceResponseWrapper)

        if response_wrapper.data.timed_out:
            raise ExecuteOnDeviceCommandTimedOutException(response_wrapper.data, response_wrapper.error_message)

        if response_wrapper.data.exit_code != 0:
            raise ExecuteOnDeviceCommandFailedException(response_wrapper.data, response_wrapper.error_message)

        return response_wrapper.data

    def read_from_device(self, request):
        """
        Read from a device. The response fields should be used only when the Read command was issued without error. They'll be empty if Read was not issued (even if there was no error).

        This reads from the serial port associated with the device.

        :param request: A :class:`.ReadFromDeviceRequest`.
        :return: A :class:`.ReadFromDeviceResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        """
        return self._make_request(request, 'readFromDevice/v1', ReadFromDeviceResponseWrapper).data

    def send_result(self, request):
        """
        Send the result of the test case to IDT.
        A result should be sent for each test case in the test suite.

        :param request: A :class:`.SendResultRequest`.
        :return: A :class:`.SendResultResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        """
        return self._make_request(request, 'sendResult/v1', SendResultResponseWrapper).data

    def poll_for_notifications(self):
        """
        Receive notifications from IDT about various events that occur.

        Test cases should continuously call this function at frequent intervals (~200ms) to receive updates about state that IDT wants to communicate with the test cases currently running.

        :return: A :class:`.PollForNotificationsResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        """
        return self._make_request(None, 'pollForNotifications/v1', PollForNotificationsResponseWrapper).data

    def get_context_value(self, request):
        """
        Get a value from IDT's context.

        Please see the request/response models for more information.

        :param request: A :class:`.GetContextValueRequest`.
        :return: A :class:`.GetContextValueResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        :raises GetContextValueUnresolvedJsonPathsException: If a JSON path can't be resolved.
        """
        response_wrapper = self._make_request(request, 'getContextValue/v1', GetContextValueResponseWrapper)

        if len(response_wrapper.data.unresolved_json_paths) != 0:
            raise GetContextValueUnresolvedJsonPathsException(response_wrapper.data, response_wrapper.error_message)

        return response_wrapper.data

    def get_context_string(self, request):
        """
        Get a string using IDT's context placeholder replacement.

        Please see the request/response models for more information.

        :param request: A :class:`.GetContextStringRequest`.
        :return: A :class:`.GetContextStringResponse`.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        :raises GetContextValueUnresolvedJsonPathsException: If a JSON path can't be resolved.
        """
        get_context_string_path = "_IDT_GetContextStringPath"

        updated_extra_defs = {
            **(request.extra_defs or {}),
            get_context_string_path: request.input_string
        }
        aux_request = GetContextValueRequest(get_context_string_path, updated_extra_defs)

        aux_response = self.get_context_value(aux_request)

        return GetContextStringResponse(
            json.loads(aux_response.value),
            aux_response.encountered_json_paths,
            aux_response.unresolved_json_paths
        )

    def _make_request(self, request, endpoint, response_type):
        """
        Performs a request.

        :param request: The request model, or None to indicate no information needs to be provided by the user.
        :param endpoint: The endpoint to make the request to.
        :param response_type: The expected response type.
        :return: The deserialized response.
        :raises DeserializationException: If the server response can't be deserialized.
        :raises Non2xxResponseException: If the server doesn't return a 2xx response.
        """
        # create converter
        converter = ModelConverter(response_type)

        # Serialize arguments
        serialized = converter.serialize(request, True) if request is not None else {}

        # Insert secret and unique id into the request
        serialized['secret'] = self._secret
        serialized['uniqueId'] = self._unique_id

        # Send the request and get a response back
        response = self._pool.request('POST',
                                      self._url + '/' + endpoint,
                                      body=json.dumps(serialized),
                                      headers={'Content-Type': 'application/json'})

        if not 200 <= response.status <= 299:
            try:
                error_message = ModelConverter(ErrorMessageResponseWrapper).deserialize(response.data).error_message
            except:
                error_message = response.data.decode()

            raise Non2xxResponseException(self._enrich_error_message(error_message, endpoint, response.status))

        return converter.deserialize(response.data)

    @staticmethod
    def _enrich_error_message(error_message, endpoint, status_code):
        """
        Adds more information to the error message, if necessary.

        :param error_message: The original error message.
        :param endpoint: The endpoint that was called.
        :param status_code: The status code of the response.
        :return: The enriched error message.
        """
        if status_code == 404:
            return f'The {endpoint} request endpoint does not exist - please check that the IDT and SDK versions in use are compatible.'

        if error_message == "":
            return f'Error code {status_code} (no more details available)'

        return error_message

    @staticmethod
    def _fetch_env_var(name, required=True):
        """
        Gets a value from the environment and provides a descriptive exception if not found and required; otherwise sets to the empty string.

        :param name: The environment variable name.
        :return: The environment variable value.
        """
        val = os.getenv(name, "")
        if val == "" and required:
            raise ValueError(f"{name} either isn't set in the environment or is empty. Make sure that you're running this test using IDT.")
        return val
