# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model, ModelConverter, TimeDeltaConverter


class ExecuteOnHostCommand(Model):
    """
    The details of the command to run on the host.
    Please note that the working directory of the process will be set to the test case directory (where test.json is located).

    :ivar str cmd: The file path of the command itself, without any arguments.
        If relative, will be considered relative to the test case directory (where test.json is located).
    :ivar args: The command-line arguments.
    :vartype args: list[string]
    :ivar env_vars: Environment variables to be set in the environment your command will run in, in addition to inheriting from IDT's environment.
        The environment variables provided here have precedence over the ones in IDT's environment in case of name conflicts.
    :vartype env_vars: dict[string, string]
    :ivar timedelta timeout: The timeout to wait for the command to complete before forcefully killing it. Leave 0 for no timeout.
        In case of timeout, any standard output/standard error will still be captured, up until the command was killed,
        and the returned exit code will not be meaningful.
    """
    _attributes = ['cmd', 'args', 'env_vars', ('timeout', TimeDeltaConverter)]

    def __init__(self, cmd, args=None, env_vars=None, timeout=0):
        self.cmd = cmd
        self.args = args
        self.env_vars = env_vars
        self.timeout = timeout


class ExecuteOnHostRequest(Model):
    """
    An execute on host request.

    :ivar command: The command to execute.
    :vartype command: :class:`.ExecuteOnHostCommand`
    :ivar str std_out_file: The absolute file path where IDT stored the process standard output.
        Useful if you did not provide one as part of the request.
    :ivar str std_err_file: The absolute file path where IDT stored the process standard error output.
        Useful if you did not provide one as part of the request.
    """
    _attributes = [('command', ModelConverter(ExecuteOnHostCommand)), 'std_out_file', 'std_err_file']

    def __init__(self, command, std_out_file=None, std_err_file=None):
        self.command = command
        self.std_out_file = std_out_file
        self.std_err_file = std_err_file


class ExecuteOnHostResponse(Model):
    """
    The response to an execute on host request.

    :ivar int exit_code: The process exit code of the command.
    :ivar bool timed_out: The standard error of the command.
    :ivar str std_out_file: The absolute file path where IDT stored the process standard output.
        Useful if you did not provide one as part of the request.
    :ivar str std_err_file: The absolute file path where IDT stored the process standard error output.
        Useful if you did not provide one as part of the request.
    """
    _attributes = ['exit_code', 'timed_out', 'std_out_file', 'std_err_file']

    def __init__(self, exit_code, timed_out, std_out_file, std_err_file):
        self.exit_code = exit_code
        self.timed_out = timed_out
        self.std_out_file = std_out_file
        self.std_err_file = std_err_file
