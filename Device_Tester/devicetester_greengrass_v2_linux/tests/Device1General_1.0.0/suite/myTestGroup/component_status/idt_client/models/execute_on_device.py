# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model, ModelConverter, BytesConverter, TimeDeltaConverter


class ExecuteOnDeviceCommand(Model):
    """
    The details of the command to run on the device.

    :ivar str command_line: The command line to execute.
    :ivar env_vars: Environment variables to be set before the command runs, where keys are environment variable names and value are the corresponding values.
        These are set only for the environment the command runs in, and changes to the environment do not persist after the command finishes.

        A note if the device under test is communicating with IDT over SSH:
          It is common for SSH servers to only allow certain environment variable names to be set, e.g. most sshd daemons on linux only allow names that start with 'LC_'.
          In order to use this parameter the SSH server on the DUT must be able to set environment variables and must accept the provided environment variable names, or the request will fail.
          If this is an issue, you can instead prepend environment variables to the start of the command to set any variable using appropriate shell syntax.
    :vartype env_vars: dict[string, string]
    :ivar timedelta timeout: A timedelta (or number of nanoseconds) to wait for the command to complete before forcefully killing it. Leave as 0 for no timeout.
        Any standard output/standard error will be returned, up until the command was killed. If the command times out, the exit code returned will not be meaningful.
    """
    _attributes = ['command_line', 'env_vars', ('timeout', TimeDeltaConverter)]

    def __init__(self, command_line, env_vars=None, timeout=0):
        self.command_line = command_line
        self.env_vars = env_vars
        self.timeout = timeout


class ExecuteOnDeviceRequest(Model):
    """
    An execute on device request.

    :ivar str command: The command to execute.
    :vartype command: :class:`.ExecuteOnDeviceCommand`
    :ivar bytes stdin: Standard input to provide to the command.
    :ivar str device_name: The name of the device to execute the command on. Leave None to use the device under test - otherwise use the name specified in the test.json for resource devices.
    """
    _attributes = [('command', ModelConverter(ExecuteOnDeviceCommand)), ('stdin', BytesConverter), 'device_name']

    def __init__(self, command, stdin=None, device_name=None):
        self.command = command
        self.stdin = stdin
        self.device_name = device_name


class ExecuteOnDeviceResponse(Model):
    """
    The response to an execute on device request.

    :ivar bytes stdout: The standard output of the command.
    :ivar bytes stderr: The standard error of the command.
    :ivar int exit_code: The exit code of the command.
    :ivar bool timed_out: Whether the command timed out or not.
    """
    _attributes = [('stdout', BytesConverter), ('stderr', BytesConverter), 'exit_code', 'timed_out']

    def __init__(self, stdout, stderr, exit_code, timed_out):
        self.stdout = stdout
        self.stderr = stderr
        self.exit_code = exit_code
        self.timed_out = timed_out
