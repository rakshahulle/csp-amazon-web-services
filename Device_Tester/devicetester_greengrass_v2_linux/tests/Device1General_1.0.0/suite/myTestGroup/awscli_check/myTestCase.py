from idt_client import *
import boto3
import time


def main():
    # Use the client SDK to communicate with IDT
    client = Client()
    
    logs = boto3.client('logs', region_name='<AWS_REGION>',
                            aws_access_key_id='<AWS_ACCESS_KEY>',
                            aws_secret_access_key='<AWS_SECRET_KEY>') 

    try:
        LOG_GROUP='DEVICE'
        LOG_STREAM='GENERAL'

        logs.create_log_group(logGroupName=LOG_GROUP)
        logs.create_log_stream(logGroupName=LOG_GROUP, logStreamName=LOG_STREAM)
    except:
        print("Log group already exists.....Using the same.....\n")

    timestamp = int(round(time.time() * 1000))
   
    
    exec_req = ExecuteOnDeviceRequest(ExecuteOnDeviceCommand("aws --version ' '"))
    
    # Run the command
    exec_resp = client.execute_on_device(exec_req)
    
    # Print the standard output
    print(exec_resp.stdout.decode("utf-8"))

    p= exec_resp.stdout.decode("utf-8")
    

     #Create a send result requestss
    sr_req = SendResultRequest(TestResult(passed=True))
    # Send the result
    client.send_result(sr_req)

    
    if "aws-cli" in p:
        sr_req = SendResultRequest(TestResult(passed=True))
        # print("passesssss")

        response = logs.put_log_events(
            logGroupName=LOG_GROUP,
            logStreamName=LOG_STREAM,
            logEvents=[

                {
                        'timestamp': timestamp,
            
                        'message':'{"TestCase":"test_aws", "Result":"passed"}'
                }
            ]       

            
        )
    else:
        sr_req = SendResultRequest(TestResult(passed=False))
        print("failled")
        response = logs.put_log_events(
            logGroupName=LOG_GROUP,
            logStreamName=LOG_STREAM,
            logEvents=[
                    {
                        'timestamp': timestamp,
            
                        'message':'{"TestCase":"test_aws", "Result":"failed"}'
                    }
            ]
        
        )


       
if __name__ == "__main__":
    main()

