# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

################################################################################

# variables
$PYTHON_INSTALLED=0
$PYTHON_VERSION_MIN="3.6.0"
$PYTHON_VERSION_RECOMMENDED="3.7.0"
$ALL_DIR_EXIST=1
$FILES_COPIED=1
$SUITE_NAME="IDTSampleSuitePython"
$SUITE_VERSION="1.0.1"
$CURRENT_DIR=$(pwd)
$PYTHON_SDK_DIR="${CURRENT_DIR}\..\..\..\sdks\python\idt_client"
$PYTHON_SUITE_SRC_DIR="${CURRENT_DIR}\..\src"
$PYTHON_SUITE_ROOT_DIR="${CURRENT_DIR}\..\configuration\${SUITE_NAME}_${SUITE_VERSION}"
$TEST_DIR="${CURRENT_DIR}\..\..\..\tests"
$SUITE_OUTPUT_DIR="${TEST_DIR}\${SUITE_NAME}_${SUITE_VERSION}"

################################################################################

# functions
function checkRequiredFilesExist {
    echo "==> Checking if all required directories exist..."

    if (-Not (Test-Path -Path ${PYTHON_SDK_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${PYTHON_SDK_DIR} directory not found."
    }

    if (-Not (Test-Path -Path ${PYTHON_SUITE_SRC_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${PYTHON_SUITE_SRC_DIR} directory not found."
    }

    if (-Not (Test-Path -Path ${PYTHON_SUITE_ROOT_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${PYTHON_SUITE_ROOT_DIR} directory not found."
    }

    if (-Not (Test-Path -Path ${TEST_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${TEST_DIR} directory not found."
    }
}

function checkPythonInstalled {
    echo "==> Checking if Python 3 is installed..."
    if (Get-Command python3) {
        echo "    Python 3 is installed!"
        $script:PYTHON_INSTALLED=1
    } Else {
        echo "    WARN: Python 3 could not be found. It must be installed and available as the python3 command. The build will continue, but in order to run the test suite Python (https://python.org/) must be installed. Recommended version >=${PYTHON_VERSION_RECOMMENDED}, Minimum version >=${PYTHON_VERSION_MIN}"
    }
}

function checkPythonVersion {
    echo "==> Checking Python 3 version..."
    $PYTHON_VERSION=(python3 --version).ToString().Split(" ")[1]

    if (${PYTHON_VERSION_MIN} -gt ${PYTHON_VERSION}) {
        echo "    WARN: At least Python $PYTHON_VERSION_MIN is required, found $PYTHON_VERSION. The build will continue, but you may not be able to run the test suite."
    } Else {
        if (${PYTHON_VERSION_RECOMMENDED} -gt ${PYTHON_VERSION}) {
            echo "    WARN: It's recommended to have at least $PYTHON_VERSION_RECOMMENDED, found $PYTHON_VERSION. The build will continue, but there may be issues."
        } Else {
            echo "    Python version $PYTHON_VERSION is at least the recommended $PYTHON_VERSION_RECOMMENDED!"
        }
    }
}

function checkPythonLibraries {
    echo "==> Checking Python 3 libraries..."
    python3 -c "import urllib3"
    if (${LASTEXITCODE} -ne 0) {
        echo "    WARN: The urllib3 library (https://pypi.org/project/urllib3/) must be installed. The build will continue, but you may not be able to run the test suite."
    } Else {
        echo "    urllib3 is installed!"
    }
}

function createSuite {
    echo "==> Preparing test suite..."

    Copy-Item -Path ${PYTHON_SUITE_ROOT_DIR} -Destination ${SUITE_OUTPUT_DIR} -Recurse
    if (${LASTEXITCODE} -ne 0) {
        $script:FILES_COPIED=0
        return
    }

    Copy-Item -Path "${PYTHON_SUITE_SRC_DIR}\*" -Destination ${SUITE_OUTPUT_DIR} -Recurse -Force
    if (${LASTEXITCODE} -ne 0) {
        $script:FILES_COPIED=0
        return
    }

    Copy-Item -Path ${PYTHON_SDK_DIR} -Destination "${SUITE_OUTPUT_DIR}\suite\sample_group" -Recurse -Force
    if (${LASTEXITCODE} -ne 0) {
        $script:FILES_COPIED=0
        return
    }
}

################################################################################

# prerequisites check
checkRequiredFilesExist

if (${ALL_DIR_EXIST} -eq 0) {
    echo "==> ERROR: Some files needed to create the test suite are missing. Please make sure this script is being run from the build scripts directory and that no files have been removed."
    exit 1
}

checkPythonInstalled

if (${PYTHON_INSTALLED} -eq 1) {
    checkPythonVersion
    checkPythonLibraries
}

# prepare the test suite
createSuite

if (${FILES_COPIED} -eq 0) {
    echo "==> ERROR: Failed to copy files. Please check above error message and make sure that you have permission access to make modifications in these two folders and all of it's sub folders."
    exit 1
}

################################################################################

Write-Host @"

################################################################################

==> Now you're ready to run the test suite if your device configuration is already set up!
==> Example commands to start your first test:
        cd ..\..\..\bin\
        .\devicetester_win_x86-64.exe run-suite --suite-id ${SUITE_NAME}_${SUITE_VERSION}

==> For more information on how to set up device configuration, please see the documentation.
"@
