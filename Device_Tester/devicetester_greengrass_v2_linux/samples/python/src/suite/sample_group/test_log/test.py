# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

import json, os

from idt_client import GetContextValueRequest, GetContextStringRequest

log_env_var_from_suite = 'LOG_FILE_PATH_FROM_SUITE'
log_env_var_from_test = 'LOG_FILE_PATH_FROM_TEST'


class LogTest:
    def __init__(self, client, log_file_path_from_test_args):
        self._client = client
        self._log_file_path_from_test_args = log_file_path_from_test_args

    # Testcase to demo the usage of the GetContextValue and GetContextString operations
    def run(self):
        # ===============================================================
        # Method 1: Use get_context_value to get a value and deserialize it to a string
        # ===============================================================
        # This is a suggested way to get a value from IDT's context.
        # A value returned from GetContextValue will be a json encoded byte array,
        # and can represent many types of json values.

        get_context_value_request = GetContextValueRequest('testData.logFilePath')
        get_context_value_response = self._client.get_context_value(get_context_value_request)

        # In this case we are expecting a string to be resolved.
        log_file_path = json.loads(get_context_value_response.value)
        assert(isinstance(log_file_path, str))

        print("Method 1, the log file path is: %s" % log_file_path)

        # ===============================================================
        # Method 2: Use get_context_value to get an object value and deserialize it to an object, then get the string
        # ===============================================================
        # This is another suggested way to get a value from IDT's context.

        # Get the entire testData object:
        get_context_value_request = GetContextValueRequest('testData')
        get_context_value_response = self._client.get_context_value(get_context_value_request)

        # Deserialize the returned object and extract the expected field:
        deserialized_object = json.loads(get_context_value_response.value)
        assert(isinstance(deserialized_object, dict))
        log_file_path = deserialized_object['logFilePath']
        assert(isinstance(log_file_path, str))

        print("Method 2, the log file path is: %s" % log_file_path)

        # ===============================================================
        # Method 3: Use GetContextString to get a string value as a string
        # ===============================================================
        # This is a suggested way to get a value from IDT's context,
        # or use IDT's placeholder replacement on a given string.

        # Unlike get_context_value, get_context_string returns a string with no deserialization needed.
        # This is convenient in our case because we're getting a string, but would not work for other types of JSON values.
        get_context_string_request = GetContextStringRequest('{{testData.logFilePath}}')
        get_context_string_response = self._client.get_context_string(get_context_string_request)
        log_file_path = get_context_string_response.value

        print("Method 3, the log file path is: %s" % log_file_path)

        # ===============================================================
        # Method 4: Use GetContextString to get a string value as a string (+ extra definitions, interpolation)
        # ===============================================================
        # This approach is essentially the same as Method 3, but it also shows how extra definitions work (also applicable to GetContextValue) and how GetContextString's string interpolation works.
        # It prints the same output as Method 3 (besides the method number), but has IDT construct the output string.

        # This is convenient if you'd like to combine multiple strings from the context together without any further string processing.
        # This has the same restrictions as Method 3 (context values must be able to be converted to strings).

        get_context_string_request = GetContextStringRequest('Method {{methodNumber}}, the log file path is: {{testData.logFilePath}}', {'methodNumber': 4})
        get_context_string_response = self._client.get_context_string(get_context_string_request)
        response_value = get_context_string_response.value

        print(response_value)

        # ===============================================================
        # Method 5: Use an environment variable set in suite.json to get a value
        # ===============================================================
        # This is not a recommended way of getting a value from IDT's context, but here for reference.
        # This method won't work for objects, arrays of anything but strings, or None values.

        # suite.json defines and environment variable as "{{testData.logFilePath}}",
        # which will be replaced with the proper value when the environment variable is set.
        log_file_path = os.getenv(log_env_var_from_suite)

        print("Method 5, the log file path is: %s" % log_file_path)

        # ===============================================================
        # Method 6: Use an environment variable set in test.json to get a value
        # ===============================================================
        # This is not a recommended way of getting a value from IDT's context, but here for reference.
        # This method won't work for objects, arrays of anything but strings, or None values.

        # test.json defines and environment variable as "{{testData.logFilePath}}",
        # which will be replaced with the proper value when the environment variable is set.
        log_file_path = os.getenv(log_env_var_from_test)

        print("Method 6, the log file path is: %s" % log_file_path)

        # ===============================================================
        # Method 7: Use a CLI argument set in test.json to get a value
        # ===============================================================
        # This is not a recommended way of getting a value from IDT's context, but here for reference.
        # This method won't work for objects, arrays of anything but strings, or None values.

        # test.json provides a CLI argument that holds "{{testData.logFilePath}}",
        # which will be replaced with the proper value before being passed as an argument.

        # The log_file_path_from_test_args argument is captured in main.py upon startup,
        # and is passed to this test case's constructor.
        log_file_path = self._log_file_path_from_test_args

        print("Method 7, the log file path is: %s" % log_file_path)

        return None

    # This test doesn't create any resources that need cleanup, so this method does nothing.
    def cleanup(self):
        pass
