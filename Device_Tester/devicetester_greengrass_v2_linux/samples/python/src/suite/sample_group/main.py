# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

import argparse
import sys
import signal

from idt_client import Client, SendResultRequest, ClientException, TestResult, FailureMessage, ErrorMessage

from test_cat.test import CatTest
from test_ram.test import RamTest
from test_ping.test import PingTest
from test_log.test import LogTest

# This suite is structured as a main file in the group folder, and then test files in the test case folders. This is just one way to structure tests - you can structure the suite however you want.


def main():
    print('Running test case', flush=True)
    # Create a client to talk to IDT
    client = Client()

    # Retrieve which test to run (see test.json)
    parser = argparse.ArgumentParser()
    parser.add_argument('test', help='Which test to run')
    parser.add_argument('--log_file_path_from_test_args', help='used by the log test case', default=None)

    args = parser.parse_args()
    if args.test == 'cat':
        test = CatTest(client)
    elif args.test == 'ram':
        test = RamTest(client)
    elif args.test == 'ping':
        test = PingTest(client)
    elif args.test == 'log':
        test = LogTest(client, args.log_file_path_from_test_args)
    else:
        print('Unrecognized test: ' + args.test)
        sys.exit(1)

    # Setup signal handler
    handle_signals(test)

    # Run the test
    test_result = run_test_and_create_result(test)

    # Clean up the test. This cleanup is best effort - if cleanup fails but the test succeeds, the test result sent to IDT is still success.
    # In that case information is still printed about the cleanup failure and the executable exits with an error status to let the user know what happened.
    cleanup_successful = cleanup_test(test)

    # Send result
    try:
        client.send_result(SendResultRequest(test_result))
    except Exception as e:
        print("Couldn't send result to IDT")
        print(e)
        sys.exit(1)

    print('Finished running test case')
    sys.exit(0 if cleanup_successful else 1)


# Runs a test and creates a test result to send to IDT.
def run_test_and_create_result(test):
    try:
        failure_message = test.run()
    except Exception as e:
        test_result = TestResult(error=ErrorMessage('Test Error', str(e)))
    else:
        if failure_message is not None:
            test_result = TestResult(failure=FailureMessage('Test Failure', failure_message))
        else:
            test_result = TestResult(passed=True)
    return test_result


# Runs test cleanup, returning whether it was successful or not.
def cleanup_test(test):
    print('Cleaning up resources')
    try:
        test.cleanup()
    except Exception as e:
        print(e)
        return False

    return True


# IDT sends an alarm signal to the test executable when it times out. After that, the test case has 5 minutes before it is forcibly killed.
# When the user presses ctrl + c in their shell, the test executable receives an interrupt signal.
# Both these cases indicate that the test should stop running. Before stopping, the test should first clean up the device it's running on.
def handle_signals(test):
    def signal_handler(sig, frame):
        print("Received exit signal, cleaning up device.")
        sys.exit(0 if cleanup_test(test) else 1)

    signal.signal(signal.SIGINT, signal_handler)
    if sys.platform == 'win32':
        signal.signal(signal.SIGBREAK, signal_handler)
    else:
        signal.signal(signal.SIGALRM, signal_handler)


if __name__ == '__main__':
    main()
