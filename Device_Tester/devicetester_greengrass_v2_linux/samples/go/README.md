# AWS IoT Device Tester Go Sample Test Suite

This sample test suite contains a set of example test cases illustrating how to create a test suite with basic usage of 
the IDT Go client SDK.

## Overview

The files provided look like this:
```
devicetester
├── ...
├── tests
├── samples
│   ├── ...
│   └── go
│       ├── configuration
│       ├── src
│       └── build-scripts
└── sdks
    ├── ...
    └── go
        └── idtclient
```

These files encompass the source code for the sample test suite, written in Go. Specifically:
* `configuration` contains JSON configuration which provide test suite meta-information to IDT.
* `src` contains the sample test suite source code - the test logic.
* `idtclient` contains the Go SDK source code, used to communicate with IDT.
* `build-scripts` contains scripts which combine the configuration, source code, and sdk to produce a runnable test suite.

The `configuration` and `src` folders can be browsed as an example of a test suite.

The `idtclient` folder can be browsed to learn about the IDT SDK and should be copied into your own test suite.

The `build-scripts` folder provides build scripts to easily assemble the sample test suite (given that the layout from the
download is not modified). When you write your test suite, you need not use these scripts and can use any build system
you like - the only requirement is producing a valid test suite folder.

## Prerequisites

#### Building

The sample test suite introduces no new prerequisites on top of the client SDK.

To build the sample test suite, Go is needed:
* Go (>=1.12) - https://golang.org/

and no third party packages need to be installed.

#### Running

No additional software is necessary; the Go test suite contains binaries compiled for Windows, Mac, and Linux.

#### Device requirements

The sample test suite tests a device over an SSH connection by executing basic shell commands:
* cat
* echo
* rm

In the sample test suite, there is one test case (test_ram) which also uses some advanced commands:
* grep
* awk
* and relies on /proc/meminfo existing.

See the getting started tutorial in the documentation for more information on how to set up a device to run the sample test suite.

## Building the sample test suite

To run the build scripts to create a runnable sample test suite:
* Open a terminal.
* Change directories to the devicetester/samples/go/build-scripts folder.
* Run the build.sh script if on a Mac/UNIX-like operating system, or build.ps1 if on a Windows operating system.

This will produce a new directory in the devicetester/tests folder which is a ready to run test suite:
* devicetester/tests/IDTSampleSuiteGo_1.0.1

## Running the sample test suite

To run the sample test suite, follow these steps:
* Open a terminal.
* Change directories to the bin folder within devicetester.
* Run ./devicetester_PLATFORM run-suite --suite-id IDTSampleSuiteGo_1.0.1

#### Example usage

```
# Build sample test suite
$ cd devicetester/samples/go/build-scripts
# Run the build script based on your os:
$ ./build.sh  # for Mac/UNIX-like os
$ ./build.ps1 # for Windows os

# Run sample test suite
$ cd ../../../bin/
# Run the IDT command based on your os:
$ ./devicetester_mac_x86-64 run-suite --suite-id IDTSampleSuiteGo_1.0.1
$ ./devicetester_linux_x86-64 run-suite --suite-id IDTSampleSuiteGo_1.0.1
$ ./devicetester_win_x86-64.exe run-suite --suite-id IDTSampleSuiteGo_1.0.1
``` 

## License

The sample test suite is licensed under the MIT-0 License.
