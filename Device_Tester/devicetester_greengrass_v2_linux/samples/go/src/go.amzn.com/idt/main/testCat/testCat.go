// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package testCat

import (
	"fmt"
	"go.amzn.com/idt/main/runTestUtils"
	"idtclient"
	"idtclient/models"
	"io/ioutil"
	"os"
	"path/filepath"
)

const (
	destFilePath = "testFile.txt"
	destDirPath = "~/testDirectory"
	destFileInDir = "~/testDirectory/testFile.txt"
)

func NewCatTest(myClient idtclient.Client) runTestUtils.Test {
	return &catTest{
		client: myClient,
	}
}

type catTest struct {
	client idtclient.Client
}

// The cat test creates a file on the device. In order to cleanup the test, it needs to be deleted.
func (c *catTest) Cleanup() error {
	/*****************************************************************************
	            Cleanup 1. Cleanup the file copied from host agent to device
	*****************************************************************************/
	myFileExecuteOnDeviceRequest := models.ExecuteOnDeviceRequest{
		Command: models.ExecuteOnDeviceCommand{
			CommandLine: fmt.Sprintf("rm %s", destFilePath),
		},
	}

	if _, err := c.client.ExecuteOnDevice(myFileExecuteOnDeviceRequest); err != nil {
		return err
	}

	/*****************************************************************************
	        Cleanup 2. Cleanup the directory copied from host agent to device
	*****************************************************************************/
	myDirExecuteOnDeviceRequest := models.ExecuteOnDeviceRequest{
		Command: models.ExecuteOnDeviceCommand{
			CommandLine: fmt.Sprintf("rm %s && rmdir %s", destFileInDir, destDirPath),
		},
	}

	if _, err := c.client.ExecuteOnDevice(myDirExecuteOnDeviceRequest); err != nil {
		return err
	}
	return nil
}

// Testcase that tests cat command's capability.
func (c *catTest) Run() (string, error) {

	/*****************************************************************************
	            Test 1. Copy a file from host agent to device
	*****************************************************************************/

	/******************************************
		Call copy to device operation.
	******************************************/
	// Note: If you don't have resource device, you can just specify uniqueId on the request, and IDT will execute command on DeviceUnderTest.
	// Create a copyToDevice request, please navigate to src/idtclient/models/copy_to_device.go to see what CopyToDeviceRequest looks like.
	curDir, _ := os.Getwd()
	localFilePath := filepath.Join(curDir, "testFile.txt")

	myCopyToDeviceRequest := models.CopyToDeviceRequest{
		SourcePath: localFilePath,
		TargetPath: destFilePath,
	}
	// Send copyToDevice request
	_, err := c.client.CopyToDevice(myCopyToDeviceRequest)
	if err != nil {
		return "", err
	}

	/******************************************
		Call execute on device operation.
	******************************************/
	// Create a execute on device request, please navigate to src/idtclient/models/execute_on_device.go to see what ExecuteOnDeviceRequest looks like.
	myExecuteOnDeviceRequest := models.ExecuteOnDeviceRequest{
		Command: models.ExecuteOnDeviceCommand{
			CommandLine: fmt.Sprintf("cat %s", destFilePath),
		},
	}
	// Send execute on device request
	myExecuteOnDeviceResponse, err := c.client.ExecuteOnDevice(myExecuteOnDeviceRequest)
	if err != nil {
		return "", err
	}
	fmt.Printf("Execute on device response: %s \n", runTestUtils.DeepPrint(myExecuteOnDeviceResponse))

	/******************************************
		Compare files to ensure that the file contents are the same.
	******************************************/
	catOutput := string(myExecuteOnDeviceResponse.StdOut)
	originalFileBytes, err := ioutil.ReadFile(localFilePath) // just pass the file name
	if err != nil {
		return "", err
	}

	originalFileStr := string(originalFileBytes)
	if originalFileStr != catOutput {
		return "Local file contents doesn't match remote contents", err
	}

	/*****************************************************************************
	           Test 2. Copy a directory from host agent to device
	*****************************************************************************/

	/******************************************
		Call copy to device operation.
	******************************************/
	// Note: If you don't have resource device, you can just specify uniqueId on the request, and IDT will execute command on DeviceUnderTest.
	// Create a copyToDevice request, please navigate to src/idtclient/models/copy_to_device.go to see what CopyToDeviceRequest looks like.

	localDirPath := filepath.Join(curDir, "testDirectory")

	myCopyDirToDeviceRequest := models.CopyToDeviceRequest{
		SourcePath: localDirPath,
		TargetPath: destDirPath,
	}
	// Send copyToDevice request
	_, err = c.client.CopyToDevice(myCopyDirToDeviceRequest)
	if err != nil {
		return "", err
	}

	/******************************************
		Call execute on device operation.
	******************************************/
	// Create a execute on device request, please navigate to src/idtclient/models/execute_on_device.go to see what ExecuteOnDeviceRequest looks like.
	myDirExecuteOnDeviceRequest := models.ExecuteOnDeviceRequest{
		Command: models.ExecuteOnDeviceCommand{
			CommandLine: fmt.Sprintf("cat %s", destFileInDir),
		},
	}
	// Send execute on device request
	myDirExecuteOnDeviceResponse, err := c.client.ExecuteOnDevice(myDirExecuteOnDeviceRequest)
	if err != nil {
		return "", err
	}
	fmt.Printf("Execute on device response: %s \n", runTestUtils.DeepPrint(myDirExecuteOnDeviceResponse))

	/******************************************
		Compare files to ensure that the file contents are the same.
	******************************************/
	localFilePathInDir := filepath.Join(localDirPath, "testFile.txt")
	catOutputInDir := string(myDirExecuteOnDeviceResponse.StdOut)
	originalFileBytesInDir, err := ioutil.ReadFile(localFilePathInDir) // just pass the file name
	if err != nil {
		return "", err
	}

	originalFileStrInDir := string(originalFileBytesInDir)
	if originalFileStrInDir != catOutputInDir {
		return "Local file contents doesn't match remote contents", err
	}


	return "", nil
}
