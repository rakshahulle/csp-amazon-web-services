# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

################################################################################

# variables
$GO_INSTALLED=0
$ALL_DIR_EXIST=1
$GO_BUILD_SUCCEEDED=1
$FILE_COPIED=1
$GO_VERSION_MIN="1.12.0"
$GO_SUITE_NAME="IDTSampleSuiteGo"
$SUITE_VERSION="1.0.1"
$CURRENT_DIR=$(pwd)
$GO_PKG_PATH="${CURRENT_DIR}\.."
$GO_SDK_PATH="${CURRENT_DIR}\..\..\..\sdks\go\idtclient"
$GO_SUITE_ROOT_DIR="${CURRENT_DIR}\..\configuration"
$TEST_DIR="${CURRENT_DIR}\..\..\..\tests"

$TEST_BIN_NAME_MAC="idt_sample_test_mac_x86_64"
$TEST_BIN_NAME_WIN="idt_sample_test_win_x86_64.exe"
$TEST_BIN_NAME_LINUX="idt_sample_test_linux_x86_64"

################################################################################

# functions
function checkRequiredFilesExist {
    echo "==> Checking if all required directories exist..."

    if (-Not (Test-Path -Path ${GO_SDK_PATH})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${GO_SDK_PATH} directory not found."
    }

    if (-Not (Test-Path -Path ${GO_SUITE_ROOT_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${GO_SUITE_ROOT_DIR} directory not found."
    }

    if (-Not (Test-Path -Path ${GO_PKG_PATH}\src)) {
        $script:ALL_DIR_EXIST=0
        echo "    ${GO_PKG_PATH}\src directory not found."
    }

    if (-Not (Test-Path -Path ${TEST_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${TEST_DIR} directory not found."
    }
}

function checkGoInstalled {
    echo "==> Checking if Go is installed..."
    if (Get-Command go) {
        $script:GO_INSTALLED=1
        echo "    Go is installed!"
    }
}

function checkGoVersion {
    echo "==> Checking Go version..."
    $GO_VERSION=(go version).ToString().Split(" ")[2].SubString(2)

    if (${GO_VERSION_MIN} -gt ${GO_VERSION}) {
        echo "    WARN: It's recommended to have at least ${GO_VERSION_MIN}, found ${GO_VERSION}. The build will continue, but there may be issues."
    } Else {
        echo "    Go version ${GO_VERSION} is at least ${GO_VERSION_MIN}!"
    }
}

function setUpFiles {
    echo "==> Preparing files before compiling..."
    # copy go sdk to existing workspace
    Copy-Item -Path ${GO_SDK_PATH} -Destination $GO_PKG_PATH\src -Recurse
    if ($LASTEXITCODE -ne 0) {
        $script:FILE_COPIED=0
        return
    }

    # set go path
    $Env:GOPATH += ";${GO_PKG_PATH}"

    Copy-Item -Path ${CURRENT_DIR}\..\configuration\${GO_SUITE_NAME}_${SUITE_VERSION} -Destination ${CURRENT_DIR}\..\..\..\tests -Recurse
    if ($LASTEXITCODE -ne 0) {
        $script:FILE_COPIED=0
        return
    }
}

function compileGoFiles {
    echo "==> Compiling sample test suite..."
    $GO_BUILD_DESTINATION="${CURRENT_DIR}\..\..\..\tests\${GO_SUITE_NAME}_${SUITE_VERSION}\suite\sample_group"
    $OS_BINS = @{"windows"=${TEST_BIN_NAME_WIN}; "linux"=${TEST_BIN_NAME_LINUX}; "darwin"=${TEST_BIN_NAME_MAC}}
    foreach ($OS_BIN in $OS_BINS.GetEnumerator()) {
        $OS = $($OS_BIN.Key)
        $BIN_NAME =$($OS_BIN.Value)
        $Env:GOOS=${OS}
        $Env:GOARCH="amd64"
        $Env:GOBIN=${GO_BUILD_DESTINATION}
        go build -o ${GO_BUILD_DESTINATION}\${BIN_NAME} ..\src\go.amzn.com\idt\main\sample.go
        if ($LASTEXITCODE -ne 0) {
            $script:GO_BUILD_SUCCEEDED=0
        }
    }
}

function cleanup {
    echo "==> Cleaning up temporary files..."
    Remove-Item -Path ${GO_PKG_PATH}\src\idtclient -Recurse
}

################################################################################

# prerequisites check
checkRequiredFilesExist
if (${ALL_DIR_EXIST} -eq 0) {
    echo "==> ERROR: Some files needed to create the test suite are missing. Please make sure this script is being run from the build scripts directory and that no files have been removed."
    exit 1
}

checkGoInstalled

if (${GO_INSTALLED} -eq 0) {
    echo "==> ERROR: Go is not installed. Please install Go (>=1.12, https://golang.org/) before running the build script."
    exit 1
}

# optional check
checkGoVersion

# prepare the test suite
setUpFiles

if (${FILE_COPIED} -eq 0) {
    echo "==> ERROR: Failed to copy files. Please check above error message and make sure that you have permission access to make modifications in these two folders and all of it's sub folders."
    exit 1
}

compileGoFiles

if (${GO_BUILD_SUCCEEDED} -eq 0) {
    echo "==> ERROR: Failed to build one or more test binaries. Please check the error message output by build command."
    exit 1
}

# clean up
cleanup

################################################################################

Write-Host @"

################################################################################

==> Now you're ready to run the test suite if your device configuration is already set up!
==> Example commands to start your first test:
        cd ..\..\..\bin\
        .\devicetester_win_x86-64.exe run-suite --suite-id ${GO_SUITE_NAME}_${SUITE_VERSION}

==> For more information on how to set up device configuration, please see the documentation.
"@
