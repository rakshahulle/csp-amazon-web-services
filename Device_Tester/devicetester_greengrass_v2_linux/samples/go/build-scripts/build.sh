#!/bin/sh

# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

################################################################################

# variables
GO_INSTALLED=0
ALL_DIR_EXIST=1
GO_BUILD_SUCCEEDED=1
FILE_COPIED=1
GO_VERSION_MIN=1.12.0
GO_SUITE_NAME=IDTSampleSuiteGo
SUITE_VERSION=1.0.1
CURRENT_DIR=$(pwd)
GO_PKG_PATH=${CURRENT_DIR}/..
GO_SDK_PATH=${CURRENT_DIR}/../../../sdks/go/idtclient
GO_SUITE_ROOT_DIR=${CURRENT_DIR}/../configuration
TEST_DIR=${CURRENT_DIR}/../../../tests

TEST_BIN_NAME_MAC=idt_sample_test_mac_x86_64
TEST_BIN_NAME_WIN=idt_sample_test_win_x86_64.exe
TEST_BIN_NAME_LINUX=idt_sample_test_linux_x86_64

################################################################################

# functions
checkRequiredFilesExist() {
    echo "==> Checking if all required directories exist..."

    if [ ! -d ${GO_SDK_PATH} ]; then
        ALL_DIR_EXIST=0
        echo "    ${GO_SDK_PATH} directory not found."
    fi

    if [ ! -d ${GO_SUITE_ROOT_DIR} ]; then
        ALL_DIR_EXIST=0
        echo "    ${GO_SUITE_ROOT_DIR} directory not found."
    fi

    if [ ! -d ${GO_PKG_PATH}/src ]; then
        ALL_DIR_EXIST=0
        echo "    ${GO_PKG_PATH}/src directory not found."
    fi

    if [ ! -d ${TEST_DIR} ]; then
        ALL_DIR_EXIST=0
        echo "    ${TEST_DIR} directory not found."
    fi
}

checkGoInstalled() {
    echo "==> Checking if Go is installed..."
    if [[ -x "$(command -v go)" ]]; then
        GO_INSTALLED=1
        echo "    Go is installed!"
    fi
}

checkGoVersion() {
    echo "==> Checking Go version..."
    GO_VERSION=$(go version | cut -d ' ' -f 3 | tr -d 'go')
    GO_VERSION_MAJOR=`echo $GO_VERSION | cut -d . -f 1`
    GO_VERSION_MINOR=`echo $GO_VERSION | cut -d . -f 2`
    GO_VERSION_REVISION=`echo $GO_VERSION | cut -d . -f 3`

    GO_VERSION_MIN_MAJOR=`echo $GO_VERSION_MIN | cut -d . -f 1`
    GO_VERSION_MIN_MINOR=`echo $GO_VERSION_MIN | cut -d . -f 2`
    GO_VERSION_MIN_REVISION=`echo $GO_VERSION_MIN | cut -d . -f 3`

    if [ ${GO_VERSION_MAJOR} -lt ${GO_VERSION_MIN_MAJOR} ] ||
        ( [ ${GO_VERSION_MAJOR} -eq ${GO_VERSION_MIN_MAJOR} ] &&
          ( [ ${GO_VERSION_MINOR} -lt ${GO_VERSION_MIN_MINOR} ] ||
            ( [ ${GO_VERSION_MINOR} -eq ${GO_VERSION_MIN_MINOR} ] &&
              [ ${GO_VERSION_REVISION} -lt ${GO_VERSION_MIN_REVISION} ] ))); then
        echo "    WARN: It's recommended to have at least $GO_VERSION_MIN, found $GO_VERSION. The build will continue, but there may be issues."
    else
        echo "    Go version $GO_VERSION is at least $GO_VERSION_MIN!"
    fi
}

setUpFiles() {
    echo "==> Preparing files before compiling..."
    # copy go sdk to existing workspace
    cp -r ${GO_SDK_PATH} $GO_PKG_PATH/src
    if [ $? -ne 0 ]; then
        FILE_COPIED=0
        return
    fi

    # set go path
    export GOPATH=${GOPATH}:${GO_PKG_PATH}

    cp -r ${CURRENT_DIR}/../configuration/${GO_SUITE_NAME}_${SUITE_VERSION} ${CURRENT_DIR}/../../../tests
    if [ $? -ne 0 ]; then
        FILE_COPIED=0
        return
    fi
}

compileGoFiles() {
    echo "==> Compiling sample test suite..."
    GO_BUILD_DESTINATION=${CURRENT_DIR}/../../../tests/${GO_SUITE_NAME}_${SUITE_VERSION}/suite/sample_group
    for i in windows,${TEST_BIN_NAME_WIN} linux,${TEST_BIN_NAME_LINUX} darwin,${TEST_BIN_NAME_MAC} ; do
        os=${i%,*};
        bin_name=${i#*,};
        env GOOS=${os} GOARCH=amd64 GOBIN=${GO_BUILD_DESTINATION} go build -o ${GO_BUILD_DESTINATION}/${bin_name} ../src/go.amzn.com/idt/main/sample.go
        if [ $? -ne 0 ]; then
            GO_BUILD_SUCCEEDED=0
        fi
    done
}

cleanup() {
    echo "==> Cleaning up temporary files..."
    rm -r ${GO_PKG_PATH}/src/idtclient
}

################################################################################

# prerequisites check
checkRequiredFilesExist

if [ ${ALL_DIR_EXIST} -eq 0 ]; then
    echo "==> ERROR: Some files needed to create the test suite are missing. Please make sure this script is being run from the build scripts directory and that no files have been removed."
    exit 1
fi

checkGoInstalled

if [ ${GO_INSTALLED} -eq 0 ]; then
    echo "==> ERROR: Go is not installed. Please install Go (>=1.12, https://golang.org/) before running the build script."
    exit 1
fi

# optional check
checkGoVersion

# prepare the test suite
setUpFiles

if [ ${FILE_COPIED} -eq 0 ]; then
    echo "==> ERROR: Failed to copy files. Please check above error message and make sure that you have permission access to make modifications in these two folders and all of it's sub folders."
    exit 1
fi

compileGoFiles

if [ ${GO_BUILD_SUCCEEDED} -eq 0 ]; then
    echo "==> ERROR: Failed to build one or more test binaries. Please check the error message output by build command."
    exit 1
fi

# clean up
cleanup

################################################################################

cat << EOF

################################################################################

==> Now you're ready to run the test suite if your device configuration is already set up!
==> Example commands to start your first test:
        cd ../../../bin/
        # select an IDT command based on your operating system:
        ./devicetester_mac_x86-64 run-suite --suite-id ${GO_SUITE_NAME}_${SUITE_VERSION}
        ./devicetester_linux_x86-64 run-suite --suite-id ${GO_SUITE_NAME}_${SUITE_VERSION}

==> For more information on how to set up device configuration, please see the documentation.
EOF
