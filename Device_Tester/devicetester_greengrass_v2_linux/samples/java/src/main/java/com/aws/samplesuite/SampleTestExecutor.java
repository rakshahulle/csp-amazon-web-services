// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package com.aws.samplesuite;

import com.amazonaws.iot.idt.IDTClient;
import com.amazonaws.iot.idt.ServiceConfiguration;
import com.amazonaws.iot.idt.exception.IDTServiceException;
import com.amazonaws.iot.idt.model.*;
import com.aws.samplesuite.testCat.TestCat;
import com.aws.samplesuite.testLog.TestLog;
import com.aws.samplesuite.testPing.TestPing;
import com.aws.samplesuite.testRam.TestRam;
import com.aws.samplesuite.test.AbstractTest;
import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * SampleTestExecutor Class.
 */
public class SampleTestExecutor {

    /**
     * main method.
     * @param args - variable(s) gets from test.json file when test case is called.
     */
    public static void main(String[] args) {
        System.out.println("Running test case");
        /******************************************
         1. Create a client to send request.
         *******************************************/

        ServiceConfiguration defaultConfig = ServiceConfiguration.builder().build();
        IDTClient client = IDTClient.create(ServiceConfiguration.builder()
                .from(defaultConfig)
                .readTimeoutInMinutes(1)
                .build());

        /******************************************
         2. Retrieve test to run from test.json.
         *******************************************/
        AbstractTest test = null;
        String testName = args[0];

        if (testName.equals("-Dtest=cat")) {
            test = new TestCat(client);
        } else if(testName.equals("-Dtest=ping")) {
            test = new TestPing(client);
        } else if(testName.equals("-Dtest=ram")) {
            test = new TestRam(client);
        } else if(testName.equals("-Dtest=log")) {
            String logFilePathPtr = args[1];
            test = new TestLog(client, logFilePathPtr);
        } else {
            System.out.println("Unrecognized test:" + testName);
            System.exit(1);
        }

        /******************************************
         3. Setup signal handler.

         IDT sends an alarm signal to the test executable when it times out. After that, the test case has 5 minutes before it is forcibly killed.
         When the user presses ctrl + c in their shell, the test executable receives an interrupt signal.
         Both these cases indicate that the test should stop running. Before stopping, the test should first clean up the device it's running on.
         *******************************************/
        handle_signals(test);

        /******************************************
         4. Run test.
         *******************************************/
        TestResult testResult = runTestAndCreateResult(test);

        /******************************************
         5. Clean up any resources the test created.

         This cleanup is best effort - if cleanup fails but the test succeeds, the test result sent to IDT is still success.
         In that case information is still printed about the cleanup failure and the executable exits with an error status to let the user know what happened.
         *******************************************/
        String cleanupMessage = cleanupTest(test);

        /******************************************
         6. Call sendResult endpoint to send result to IDT server.
         *******************************************/
        try {
            // Send sendResult
            SendResultResponse sendResultResponse = client.sendResult(SendResultRequest.builder()
                    .testResult(testResult)
                    .build());
            System.out.println(String.format("Send result response: %s", sendResultResponse.toString()));
        }
        catch (IDTServiceException e) {
            System.out.println(String.format("Couldn't send result to IDT with error message %s", e.getMessage()));
            System.exit(1);
        }

        // If a cleanup error occurred, exit with a non-zero status code to let the user know something went wrong. The logs will have error details.
        if(cleanupMessage != "") {
            System.exit(1);
        }
        System.out.println("Finished running test case");
    }

    /**
     * handle_signals method.
     * IDT sends an alarm signal to the test executable when it times out.
     * After that, the test case has 5 minutes before it is forcibly killed.
     * When the user presses ctrl + c in their shell,
     * the test executable receives an interrupt signal.
     * Both these cases indicate that the test should stop running.
     * Before stopping, the test should first clean up the device it's running on.
     * @param test - test instance.
     */
    public static void handle_signals(AbstractTest test) {

        SignalHandler handler = new SignalHandler () {
            @Override
            public void handle(Signal sig) {
                System.out.println("Received exit signal, cleaning up device.");
                String cleanupExitMessage = test.cleanup();
                if(cleanupExitMessage != "") {
                    System.out.println(String.format("Failed to cleanup test resource with error: %s", cleanupExitMessage));
                    System.exit(1);
                }
                System.exit(0);
            }
        };
        Signal.handle(new Signal("INT"), handler);
        Signal.handle(new Signal("TERM"), handler);
        String OSName = System.getProperty("os.name");
        if(OSName.startsWith("Mac") || OSName.startsWith("Linux")){
            Signal.handle(new Signal("ALRM"), handler);
        }
    }

    /**
     * runTestAndCreateResult method.
     * Runs a test and creates a test result to send to IDT.
     * @param test - test instance.
     * @return created test result which will be sent to IDT.
     */
    static TestResult runTestAndCreateResult(AbstractTest test) {

        boolean testPassed;
        FailureMessage failureMessage = null;
        ErrorMessage errorMessage = null;
        try {
            String runExitMessage  = test.run();
            if(runExitMessage != "") {
                testPassed = false;
                failureMessage = FailureMessage.builder()
                        .type("Test Failure")
                        .message(runExitMessage)
                        .build();
            } else {
                testPassed = true;
            }
        }
        catch (Exception e){
            testPassed = false;
            errorMessage = ErrorMessage.builder()
                    .type("Test Error")
                    .message(e.getMessage())
                    .build();
        }

        TestResult testResult = TestResult.builder()
                .passed(testPassed)
                .failure(failureMessage)
                .error(errorMessage)
                .build();

        return testResult;
    }


    /**
     * cleanupTest method.
     * Runs test cleanup, returning whether it was successful or not.
     * @param test - test instance.
     * @return information about cleanup which failed to finish.
     */
    public static String cleanupTest(AbstractTest test) {
        System.out.println("Cleaning up test resources");
        String cleanupExitMessage = test.cleanup();
        if(cleanupExitMessage != "") {
            System.out.println(String.format("Error cleaning up test case: %s", cleanupExitMessage));
        }
        return cleanupExitMessage;
    }
}