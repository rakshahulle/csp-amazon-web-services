// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package com.aws.samplesuite.test;

import com.amazonaws.iot.idt.IDTClient;

/**
 * TestUtils.
 */
public abstract class AbstractTest {
    protected final IDTClient idt;

    public AbstractTest(IDTClient idt) { this.idt = idt; }

    /**
     * run method.
     * @throws Exception - information about why the test run into error.
     * @return information about why the test did not pass.
     */
    public abstract String run() throws Exception;

    /**
     * cleanup method.
     * @return information about cleanup which failed to finish.
     */
    public abstract String cleanup();
}
