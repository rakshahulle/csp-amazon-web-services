# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

################################################################################

# variables
$JAVA_INSTALLED=0
$MAVEN_INSTALLED=0
$ALL_DIR_EXIST=1
$FILES_COPIED=1
$JAR_FILE_GENERATED=1
$JAVA_VERSION_MIN=8
$SUITE_NAME="IDTSampleSuiteJava"
$SUITE_VERSION="1.0.1"
$CURRENT_DIR=$(pwd)
$JAVA_PKG_DIR="${CURRENT_DIR}\.."
$JAVA_SDK_ROOT_DIR="${CURRENT_DIR}\..\..\..\sdks\java"
$JAVA_SDK_DIR="${JAVA_SDK_ROOT_DIR}\idtclient"
$JAVA_SUITE_SRC_DIR="${CURRENT_DIR}\..\src"
$JAVA_SUITE_ROOT_DIR="${CURRENT_DIR}\..\configuration\${SUITE_NAME}_${SUITE_VERSION}"
$TEST_DIR="${CURRENT_DIR}\..\..\..\tests"
$SUITE_OUTPUT_DIR="${TEST_DIR}\${SUITE_NAME}_${SUITE_VERSION}"

################################################################################

# functions
function checkRequiredFilesExist {
    echo "==> Checking if all required directories exist..."

    if (-Not (Test-Path -Path ${JAVA_SDK_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${JAVA_SDK_DIR} directory not found."
    }

    if (-Not (Test-Path -Path ${JAVA_SUITE_ROOT_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${JAVA_SUITE_ROOT_DIR} directory not found."
    }

    if (-Not (Test-Path -Path ${JAVA_PKG_DIR}\src)) {
        $script:ALL_DIR_EXIST=0
        echo "    ${JAVA_PKG_DIR}\src directory not found."
    }

    if (-Not (Test-Path -Path ${TEST_DIR})) {
        $script:ALL_DIR_EXIST=0
        echo "    ${TEST_DIR} directory not found."
    }
}

function checkJavaInstalled() {
    echo "==> Checking if Java is installed..."
    if (Get-Command java){
        $script:JAVA_INSTALLED=1
        echo "    Java is installed!"
    }
}

function checkJavaVersion() {
    echo "==> Checking Java version..."
    $JAVA_VERSION=(Get-Command java | Select-Object -ExpandProperty Version).ToString().Split(".")[0]
    if (${JAVA_VERSION} -lt ${JAVA_VERSION_MIN}) {
        echo "    WARN: It's recommended to have at least Java $JAVA_VERSION_MIN, found $JAVA_VERSION_MAJOR.The build will continue, but there may be issues."
    } else {
        echo "    Java version $JAVA_VERSION is at least $JAVA_VERSION_MIN!"
    }
}

function checkBuildDependencies() {
    echo "==> Checking build dependencies..."
    if (Get-Command mvn) {
        $script:MAVEN_INSTALLED=1
        echo "    Maven is installed!"
    }
}

function setUpFiles() {
    echo "==> Preparing files before compiling..."

    # copy test suite to tests folder
    Copy-Item -Path "${CURRENT_DIR}\..\configuration\${SUITE_NAME}_${SUITE_VERSION}" -Destination ${SUITE_OUTPUT_DIR} -Recurse
    if ($LASTEXITCODE -ne 0) {
        $script:FILES_COPIED=0
        return
    }
}

function compileJavaFiles() {
    echo "==> Compiling sample test suite..."
    $JAVA_SDK_JAR="${JAVA_SDK_ROOT_DIR}\target\Bridgekeeper-ClientSDK-Java-1.0.jar"
    $Java_SUITE_JAR="${JAVA_PKG_DIR}\target\JavaAmzn-IDTSampleTestSuite-1.0.jar"
    $JAVA_BUILD_DESTINATION="${CURRENT_DIR}\..\..\..\tests\${SUITE_NAME}_${SUITE_VERSION}\suite\sample_group"

    # build SDK package to generate Bridgekeeper-ClientSDK-Java-1.0.jar
    (Set-Location -Path ${JAVA_SDK_ROOT_DIR}); (mvn install)
    if (-Not (Test-Path -Path ${JAVA_SDK_JAR})) {
        echo "    ${JAVA_SDK_JAR} not found, build failed."
        $script:JAR_FILE_GENERATED=0
        return
    }

    # build sample suite package to generate JavaAmzn-IDTSampleTestSuite-1.0.jar
    (Set-Location -Path ${JAVA_PKG_DIR}); (mvn install)
    if (-Not (Test-Path -Path ${Java_SUITE_JAR})) {
      echo "    ${Java_SUITE_JAR} not found, build failed."
      $script:JAR_FILE_GENERATED=0
      return
    }

    Copy-Item -Path ${Java_SUITE_JAR} -Destination "${CURRENT_DIR}\..\..\..\tests\${SUITE_NAME}_${SUITE_VERSION}"
}

function cleanup() {
    echo "==> Cleaning up temporary files..."
    Remove-Item -Path ${JAVA_SDK_ROOT_DIR}\target -Recurse
    Remove-Item -Path ${JAVA_PKG_DIR}\target -Recurse
}
################################################################################

# prerequisites check
checkRequiredFilesExist
if (${ALL_DIR_EXIST} -eq 0) {
    echo "==> ERROR: Some files needed to create the test suite are missing. Please make sure this script is being run from the build scripts directory and that no files have been removed."
    exit 1
}

checkJavaInstalled
if (${JAVA_INSTALLED} -eq 0) {
    echo "==> ERROR: Java is not installed. Please install Java (>=8) before running the build script."
    exit 1
} else {
    checkJavaVersion
}

checkBuildDependencies
if (${MAVEN_INSTALLED} -eq 0) {
   echo "==> ERROR: Maven is not installed. Please install Maven before running the build script."
   exit 1
}

# prepare the test suite
setUpFiles
if (${FILES_COPIED} -eq 0) {
    echo "==> ERROR: Failed to copy files. Please check above error message and make sure that you have permission access to make modifications in the folder and all of it's sub folders."
    exit 1
}

compileJavaFiles
if (${JAR_FILE_GENERATED} -eq 0) {
    echo "==> ERROR: Failed to build packages. Please check above error message and rerun the script after error fixed."
    exit 1
}
# clean up
cleanup

 ################################################################################

Write-Host @"

################################################################################

==> Now you're ready to run the test suite if your device configuration is already set up!
==> Example commands to start your first test:
      cd ..\..\..\bin\
      .\devicetester_win_x86-64.exe run-suite --suite-id ${SUITE_NAME}_${SUITE_VERSION}

==> For more information on how to set up device configuration, please see the documentation.
"@
