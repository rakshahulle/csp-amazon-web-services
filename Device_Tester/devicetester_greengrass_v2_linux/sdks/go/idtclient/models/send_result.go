// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package models

// A send result request.
type SendResultRequest struct {
	// The result of the test.
	TestResult TestResult `json:"testResult"`
}

// The result of a test case execution.
//
// In order to send a well formed request, exactly one of the following must be true:
//   - failure is not nil
//   - error is not nil
//   - skipped is not nil
//   - passed is true
type TestResult struct {
	// The category name of the test case that ran.
	// Will map to JUnit's "classname" attribute in the final test report.
	// If left blank, will default to the concatenation of the suite ID and group ID of the test case.
	TestCategoryName string `json:"testCategoryName"`

	// The name of the test case that ran.
	// If left blank, will default to test case ID.
	TestCaseName string `json:"testCaseName"`

	// Info about the test case failure. Leave nil if the test didn't fail.
	Failure *FailureMessage `json:"failure"`

	// Info about the test case error. Leave nil if the test didn't error.
	Error *ErrorMessage `json:"error"`

	// Info describing why the test case was skipped. Leave nil if the test wasn't skipped.
	Skipped *SkippedMessage `json:"skipped"`

	// Indication that the test was successfully executed.
	Passed bool `json:"passed"`

	// Execution time of the test case in seconds.
	// If set to nil, test case execution time will be omitted in the generated report.
	// If set to 0, test case execution time will show in the report and be set to the elapsed time since the test case executable was launched.
	// Otherwise, test case execution time will show in the report as this value.
	//
	// Note: regardless of the above, test group execution time will show in the generated report as the elapsed time all test case executables in the group took to run.
	Duration *int `json:"duration"`
}

// Info about a test failure. This should be used when the test logic runs, but assertions/checks fail.
type FailureMessage struct {
	// The type of the failure.
	Type string `json:"type"`

	// More details about the failure.
	Message string `json:"message"`
}

// Info about a test skip. This should be used if the test decided to skip itself.
type SkippedMessage struct {
	// More details about the skip.
	Message string `json:"message"`
}

// Info about a test error. This should be used when something prevents the test logic from running.
type ErrorMessage struct {
	// The type of the error.
	Type string `json:"type"`

	// More details about the error.
	Message string `json:"message"`
}

// The response to a send result request.
type SendResultResponse struct {
	// IDT does not include any information in the response.
	// A call without an error indicates the result was received successfully.
}
