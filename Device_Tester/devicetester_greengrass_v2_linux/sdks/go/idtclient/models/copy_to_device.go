// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package models

// A copy to device request.
type CopyToDeviceRequest struct {
	// The name of the device to copy to.
	// Leave empty to use the device under test. Otherwise, use the name specified in the test.json for resource devices.
	DeviceName string `json:"deviceName"`

	// The absolute source path to the file or directory to copy.
	// Note:
	// - If this is a file, then the target path must also be a file.
	// - If this is a directory, then the target path must also be a directory.
	SourcePath string `json:"sourcePath"`

	// The absolute target path to where the file or directory should be copied.
	// Note:
	// - When copying files: if the target file already exists it will be overwritten.
	// - When copying directories: the target directory must not already exist.
	// - If IDT is running on Windows and if the target path contains '/', all '\' will be replaced with '/' to ensure cross platform copy succeeds.
	TargetPath string `json:"targetPath"`

	// Whether to use the sudo command to do the copy. Sudo must be present on the remote device if this is true.
	UseSudo bool `json:"useSudo"`
}

// The response to a copy to device request.
type CopyToDeviceResponse struct {
	// IDT does not include any information in the response.
	// A call without an error indicates the file or directory was copied successfully.
}
