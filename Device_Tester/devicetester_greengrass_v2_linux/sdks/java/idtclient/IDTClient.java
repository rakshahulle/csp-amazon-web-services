// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt;

import com.amazonaws.iot.idt.exception.IDTClientException;
import com.amazonaws.iot.idt.exception.IDTServiceException;
import com.amazonaws.iot.idt.model.CopyToDeviceRequest;
import com.amazonaws.iot.idt.model.CopyToDeviceResponse;
import com.amazonaws.iot.idt.model.ExecuteOnDeviceRequest;
import com.amazonaws.iot.idt.model.ExecuteOnDeviceResponse;
import com.amazonaws.iot.idt.model.ExecuteOnHostRequest;
import com.amazonaws.iot.idt.model.ExecuteOnHostResponse;
import com.amazonaws.iot.idt.model.GetContextStringRequest;
import com.amazonaws.iot.idt.model.GetContextStringResponse;
import com.amazonaws.iot.idt.model.GetContextValueRequest;
import com.amazonaws.iot.idt.model.GetContextValueResponse;
import com.amazonaws.iot.idt.model.PollForNotificationsRequest;
import com.amazonaws.iot.idt.model.PollForNotificationsResponse;
import com.amazonaws.iot.idt.model.ReadFromDeviceResponse;
import com.amazonaws.iot.idt.model.ReadFromDeviceRequest;
import com.amazonaws.iot.idt.model.SendResultRequest;
import com.amazonaws.iot.idt.model.SendResultResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import retrofit2.Call;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The IDT client.
 * The client that interacts with the IDT server. Sends requests and receives responses.
 */
public interface IDTClient {
    /**
     * Copy a file or directory from the machine running IDT to a device.
     * The device connection type must be SSH to copy files or directories to it.
     * The cat command must be available on the device to copy files.
     * The mkdir and cat commands must be available to copy directories.
     *
     * @param request See the CopyToDeviceRequest model for more information.
     * @return See the CopyToDeviceResponse model for more information.
     * @throws IDTServiceException
     */
    CopyToDeviceResponse copyToDevice(CopyToDeviceRequest request) throws IDTServiceException;

    /**
     * Send the result of the test to IDT.
     * A result should be sent for each test case in the test suite.
     *
     * @param request See the SendResultRequest model for more information.
     * @return See the SendResultResponse model for more information.
     * @throws IDTServiceException
     */
    SendResultResponse sendResult(SendResultRequest request) throws IDTServiceException;

    /**
     * Execute a shell command on the local machine.
     *
     * @param request See the ExecuteOnHostRequest model for more information.
     * @return See the ExecuteOnHostResponse model for more information.
     * @throws IDTServiceException
     */
    ExecuteOnHostResponse executeOnHost(ExecuteOnHostRequest request) throws IDTServiceException;

    /**
     * Execute a shell command on a device.
     *
     * @param request See the ExecuteOnDeviceRequest model for more information.
     * @return See the ExecuteOnDeviceResponse model for more information.
     * @throws IDTServiceException
     */
    ExecuteOnDeviceResponse executeOnDevice(ExecuteOnDeviceRequest request) throws IDTServiceException;

    /**
     * Receive notifications from IDT about various events that occur.
     * Test cases should continuously call this function at frequent intervals (~200ms)
     * to receive updates about state that IDT wants to communicate with the test cases currently running.
     *
     * @return See the PollForNotificationsResponse model for more information.
     * @throws IDTServiceException
     */
    PollForNotificationsResponse pollForNotifications() throws IDTServiceException;

    /**
     * Read from a device.
     * The response fields should be used only when the Read command was issued without error.
     * They'll be empty if Read was not issued (even if there was no error).
     * This reads from the serial port associated with the device.
     *
     * @param request See the ReadFromDeviceRequest model for more information.
     * @return See the ReadFromDeviceResponse model for more information.
     * @throws IDTServiceException
     */
    ReadFromDeviceResponse readFromDevice(ReadFromDeviceRequest request) throws IDTServiceException;

    /**
     * Get a value from IDT's context.
     *
     * @param request See the GetContextValueRequest model for more information.
     * @return See the GetContextValueResponse model for more information.
     * @throws IDTServiceException
     */
    GetContextValueResponse getContextValue(GetContextValueRequest request) throws IDTServiceException;

    /**
     * Get a string using IDT's context placeholder replacement.
     *
     * @param request See the GetContextStringRequest model for more information.
     * @return See the GetContextStringResponse model for more information.
     * @throws IDTServiceException
     */
    GetContextStringResponse getContextString(GetContextStringRequest request) throws IDTServiceException;

    /**
     * Creates a new IDT client.
     * @param configuration IDT server configurations, if no specific value configured,
     *                      the default configuration will be used to create an IDT client.
     *                      See the IDTServiceConfiguration for more information.
     * @return An IDTClient
     */
    static IDTClient create(ServiceConfiguration configuration) {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        final IDTService service = IDTService.create(mapper, configuration);
        final String idtContextPath = "_IDT_GetContextStringPath";
        return new IDTClient() {

            @Override
            public CopyToDeviceResponse copyToDevice(CopyToDeviceRequest request) throws IDTServiceException {
                return call(service.copyToDevice(request));
            }

            @Override
            public SendResultResponse sendResult(SendResultRequest request) throws IDTServiceException {
                return call(service.sendResult(request));
            }

            @Override
            public ExecuteOnHostResponse executeOnHost(ExecuteOnHostRequest request) throws IDTServiceException {
                return call(service.executeOnHost(request));
            }

            @Override
            public ExecuteOnDeviceResponse executeOnDevice(ExecuteOnDeviceRequest request) throws IDTServiceException {
                return call(service.executeOnDevice(request));
            }

            @Override
            public PollForNotificationsResponse pollForNotifications() throws IDTServiceException {
                return call(service.pollForNotifications(PollForNotificationsRequest.builder().build()));
            }

            @Override
            public ReadFromDeviceResponse readFromDevice(ReadFromDeviceRequest request) throws IDTServiceException {
                return call(service.readFromDevice(request));
            }

            @Override
            public GetContextValueResponse getContextValue(GetContextValueRequest request) throws IDTServiceException {
                return call(service.getContextValue(request));
            }

            @Override
            public GetContextStringResponse getContextString(GetContextStringRequest request)
                    throws IDTServiceException {
                final Map<String, String> extraDefs = Optional.ofNullable(request.extraDefs()).orElseGet(HashMap::new);
                extraDefs.put(idtContextPath, request.inputString());
                final GetContextValueResponse response = getContextValue(GetContextValueRequest.builder()
                        .jsonPath(idtContextPath)
                        .extraDefs(extraDefs)
                        .build());
                try {
                    final JsonNode unmarshall = mapper.readTree(response.value());
                    return GetContextStringResponse.builder()
                            .value(unmarshall.asText())
                            .unresolvedJsonPaths(response.unresolvedJsonPaths())
                            .encounteredJsonPaths(response.encounteredJsonPaths())
                            .build();
                } catch (IOException e) {
                    throw new IDTClientException(e);
                }
            }

            private <T> T call(Call<T> call) {
                try {
                    return call.execute().body();
                } catch (IOException ie) {
                    throw new IDTClientException(ie);
                }
            }
        };
    }

    static IDTClient defaultClient() {
        return create(ServiceConfiguration.builder().build());
    }
}
