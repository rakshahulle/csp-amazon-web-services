// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt;

import com.amazonaws.iot.idt.model.IDTModel;
import org.immutables.value.Value;

import java.util.Objects;

@Value.Immutable
@IDTModel
interface IDTServiceConfiguration {
    @Value.Default
    default long readTimeoutInMinutes() {
        return Long.parseLong(System.getProperty("idt.read.timeout.minutes", "1"));
    }

    @Value.Default
    default String secret() {
        return Objects.requireNonNull(System.getenv("IDT_SECRET"), "Need to supply a secret!");
    }

    @Value.Default
    default String uniqueId() {
        return Objects.requireNonNull(System.getenv("IDT_UNIQUE_ID"), "Need to supply a unique id!");
    }

    @Value.Default
    default int port() {
        return Integer.parseInt(Objects.requireNonNull(System.getenv("IDT_SERVER_PORT")));
    }
}
