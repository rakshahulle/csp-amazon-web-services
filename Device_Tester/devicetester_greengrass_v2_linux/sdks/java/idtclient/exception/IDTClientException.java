package com.amazonaws.iot.idt.exception;

public class IDTClientException extends IDTServiceException {
    private static final long serialVersionUID = -3259338164366829257L;

    public IDTClientException(Throwable ex) {
        super(ex);
    }
}
