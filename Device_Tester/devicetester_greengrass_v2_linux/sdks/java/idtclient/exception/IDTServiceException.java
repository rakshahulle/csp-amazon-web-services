package com.amazonaws.iot.idt.exception;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

public class IDTServiceException extends RuntimeException {
    private static final long serialVersionUID = 1471769317242552029L;
    private int statusCode;
    private Map<String, List<String>> headers;

    public IDTServiceException(
            final String message,
            final Map<String, List<String>> headers,
            final int statusCode) {
        super(message);
        this.statusCode = statusCode;
        this.headers = headers;
    }

    public IDTServiceException(final Throwable ex) {
        super(ex);
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Nullable
    public Map<String, List<String>> getHeaders() {
        return headers;
    }
}
