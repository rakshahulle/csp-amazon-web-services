// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * Info about a test skip. This should be used if the test decided to skip itself.
 */
@Value.Immutable
@IDTModel
@JsonSerialize(as = SkippedMessage.class)
interface IDTSkippedMessage {

    /**
     * More details about the skip.
     */
    String message();
}
