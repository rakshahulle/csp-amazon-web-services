// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * Info about a test error.
 * This should be used when something prevents the test logic from running.
 */
@Value.Immutable
@IDTModel
@JsonSerialize(as = ErrorMessage.class)
interface IDTErrorMessage {

    /**
     * The type of the error.
     */
    String type();

    /**
     * More details about the error.
     */
    String message();
}
