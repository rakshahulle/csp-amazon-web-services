// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * A send result request.
 */
@Value.Immutable
@IDTModel
@JsonSerialize(as = SendResultRequest.class)
interface IDTSendResultRequest {

    /**
     * The result of the test.
     */
    TestResult testResult();
}
