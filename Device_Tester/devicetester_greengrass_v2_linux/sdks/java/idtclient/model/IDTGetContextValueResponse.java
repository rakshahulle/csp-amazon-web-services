// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;

@IDTModel
@Value.Immutable
@JsonDeserialize(builder = GetContextValueResponse.Builder.class)
interface IDTGetContextValueResponse {
    // The response to a get context value request.

    /**
     * A JSON encoded byte slice which is the result of getting the value from the context.
     * This can be unmarshalled into the concrete type which it represents,
     * using e.g. standard json.Unmarshal().
     * If an error occurred, this field should not be used.
     *
     * This is the main result of the request.
     */
    byte[] value();

    /**
     * JSON paths encountered while getting the value.
     * This includes paths in any placeholders as well as the query itself.
     *
     * If an error occurred, may contain useful information or may be nil depending on the error.
     * This is optional metadata to be used if needed.
     * E.g. This can be used to validate the presence of certain placeholders.
     */
    List<String> encounteredJsonPaths();

    /**
     * A subset of EncounteredJsonPaths which contains paths whose values could not be resolved.
     * Possible reasons for resolution failure include circular references or
     * attempting to coerce types to a string that can't be coerced.
     *
     * If an error occurred, may contain useful information or may be nil depending on the error.
     * This is optional metadata to be used if needed.
     * E.g. This can be used to construct a different error message than the one IDT provides.
     */
    List<String> unresolvedJsonPaths();
}
