// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

/**
 * A read from device request.
 */
@IDTModel
@Value.Immutable
@JsonSerialize(as = ReadFromDeviceRequest.class)
interface IDTReadFromDeviceRequest {

    /**
     * The command to send. Must be a ReadFromDeviceCommand.
     * Command info:
     *   - Open causes the underlying connection to be opened. This must be issued before Read.
     *   - Read reads data from the underlying connection and should be called only after Open.
     *     Read can be called many times to keep reading data.
     *   - Close causes the underlying connection to be closed.
     *     No more Reads should be issued before issuing another Open.
     * IDT will call Close at the end of each test case if the connection is not already closed.
     */
    ReadFromDeviceCommand command();

    /**
     * The name of the device to read from.
     * Leave empty to use the device under test.
     * Otherwise, use the name specified in the test.json for resource devices.
     */
    @Nullable
    String deviceName();
}
