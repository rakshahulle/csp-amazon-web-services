// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@IDTModel
@JsonDeserialize(builder = ExecuteOnHostResponse.Builder.class)
interface IDTExecuteOnHostResponse {
    // The response to an execute on host request.

    /**
     * The process exit code of the command.
     */
    int exitCode();

    /**
     * Whether the command timed out or not.
     */
    boolean timedOut();

    /**
     * The absolute file path where IDT stored the process standard output.
     * Useful if you did not provide one as part of the request.
     */
    String stdOutFile();

    /**
     * The absolute file path where IDT stored the process standard error output.
     * Useful if you did not provide one as part of the request.
     */
    String stdErrFile();
}
