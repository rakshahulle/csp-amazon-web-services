// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

public enum ReadFromDeviceCommand {
    Open,
    Read,
    Close;
}
