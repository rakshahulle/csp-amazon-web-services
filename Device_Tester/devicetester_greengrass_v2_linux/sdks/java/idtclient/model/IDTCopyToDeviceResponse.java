// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

/**
 * The response to a copy to device request.
 */
@Value.Immutable
@IDTModel
@JsonDeserialize(builder = CopyToDeviceResponse.Builder.class)
interface IDTCopyToDeviceResponse {
    // IDT does not include information in the response
    // A call without an error indicates the file or directory was copied successfully.
}
