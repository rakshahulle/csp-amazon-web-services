// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@IDTModel
@JsonSerialize(as = ExecuteOnDeviceRequest.class)
interface IDTExecuteOnDeviceRequest {
    // An execute on device request.

    /**
     * The command to execute.
     */
    ExecuteOnDeviceCommand command();

    /**
     * Standard input to provide to the command.
     */
    @Nullable
    byte[] stdin();

    /**
     * The name of the device to execute the command on.
     * Leave empty to use the device under test.
     * Otherwise, use the name specified in the test.json for resource devices.
     */
    @Nullable
    String deviceName();
}
