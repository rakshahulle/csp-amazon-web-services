// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

@Value.Immutable
@IDTModel
@JsonSerialize(as = ExecuteOnHostCommand.class)
interface IDTExecuteOnHostCommand {
    // The details of the command to run on the host.
    // Please note that the working directory of the process will be set to
    // the test case directory (where test.json is located).

    /**
     * The file path of the command itself, without any arguments.
     * If consists of a file name only,
     * IDT will look it up using its PATH environment variable.
     * If consists of a relative path,
     * will be considered relative to the test case directory (where test.json is located).
     * Please note that relative paths are not supported in debug-test-suite mode.
     */
    @Value.Parameter
    String cmd();

    /**
     * The timeout to wait for the command to complete before forcefully killing it.
     * Leave 0 for no timeout.
     * In case of timeout, any standard output/standard error will still be captured,
     * up until the command was killed,and the returned exit code will not be meaningful.
     */
    @Value.Default
    default long timeout() {
        return 0L;
    }

    /**
     * The command-line arguments.
     */
    @Value.Parameter
    @Nullable
    List<String> args();

    /**
     * Environment variables to be set in the environment your command will run in,
     * in addition to inheriting from IDT's environment.
     * The environment variables provided here
     * have precedence over the ones in IDT's environment in case of name conflicts.
     */
    @Nullable
    Map<String, String> envVars();
}
