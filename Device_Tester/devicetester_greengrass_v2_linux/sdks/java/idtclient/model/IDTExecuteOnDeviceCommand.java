// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * The details of the command to run on the device.
 */
@Value.Immutable
@IDTModel
@JsonSerialize(as = ExecuteOnDeviceCommand.class)
interface IDTExecuteOnDeviceCommand {

    /**
     * The command line to execute.
     */
    @Value.Parameter
    String commandLine();

    /**
     * Environment variables to be set before the command runs,
     * where keys are environment variable names and value are the corresponding values.
     * These are set only for the environment the command runs in,
     * and changes to the environment do not persist after the command finishes.
     *
     * A note if the device under test is communicating with IDT over SSH:
     * - It is common for SSH servers to only allow certain environment variable names to be set,
     *   e.g. most sshd daemons on linux only allow names that start with 'LC_'.
     * - In order to use this parameter the SSH server on the DUT must be able to set environment variables
     *   and must accept the provided environment variable names, or the request will fail.
     * - If this is an issue, you can instead prepend environment variables to the start of the command
     *   to set any variable using appropriate shell syntax.
     */
    @Nullable
    Map<String, String> envVars();

    /**
     * The timeout to wait for the command to complete before forcefully killing it.
     * Leave as 0 for no timeout
     * Any standard output/standard error will be returned,
     * up until the command was killed.
     * If the command times out, the exit code returned will not be meaningful.
     */
    @Value.Default
    default long timeout() {
        return 0L;
    }
}
