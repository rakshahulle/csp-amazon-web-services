// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

/**
 * The response to a send result request.
 */
@Value.Immutable
@IDTModel
@JsonDeserialize(builder = SendResultResponse.Builder.class)
interface IDTSendResultResponse {
    // IDT does not include any information in the response.
    // A call without an error indicates the result was received successfully.
}
