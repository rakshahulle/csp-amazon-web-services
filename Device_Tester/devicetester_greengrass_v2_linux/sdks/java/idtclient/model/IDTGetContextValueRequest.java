// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Map;

@IDTModel
@Value.Immutable
@JsonSerialize(as = GetContextValueRequest.class)
interface IDTGetContextValueRequest {
    // A get context value request.

    /**
     * The JSON Path for the value to get from the context.
     * When getting anything but the root object, the leading "$." is optional.
     */
    String jsonPath();

    /**
     * Optional extra definitions to insert into the IDT context for just this request.
     * These definitions are merged at the root of the context object.
     *
     * For example, if {"foo" : "bar"} is provided here,
     * then "foo" is now a valid JSON path to query in the IDT context even though IDT does not provide that path.
     * It will be resolved to "bar".
     *
     * Note:
     * - Using keys IDT already provides (e.g. testData) should be avoided.
     *   If there is a key collision, values will resolve to the extra definition value and not IDT's value.
     * - Keys must not start with "_IDT_" - these are reserved for internal use.
     */
    @Nullable
    Map<String, String> extraDefs();
}
