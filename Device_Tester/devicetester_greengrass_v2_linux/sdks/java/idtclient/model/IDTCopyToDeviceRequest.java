// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

/**
 * A copy to device request.
 */
@Value.Immutable
@IDTModel
@JsonSerialize(as = CopyToDeviceRequest.class)
interface IDTCopyToDeviceRequest {

    /**
     * The absolute source path to the file or directory to copy.
     * Note:
     * - If this is a file, then the target path must also be a file.
     * - If this is a directory, then the target path must also be a directory.
     */
    @Value.Parameter
    String sourcePath();

    /**
     * The absolute target path to where the file or directory should be copied.
     * Note:
     * - When copying files: if the target file already exists it will be overwritten.
     * - When copying directories: the target directory must not already exist.
     * - If IDT is running on Windows and if the target path contains '/',
     *   all '\' will be replaced with '/' to ensure cross platform copy succeeds.
     */
    @Value.Parameter
    String targetPath();

    /**
     * Whether to use the sudo command to do the copy.
     * Sudo must be present on the remote device if this is true.
     * @return whether use sudo to do the copy, default value is false.
     */
    @Value.Default
    default boolean useSudo() {
        return false;
    }


    /**
     * The name of the device to copy to.
     * Leave empty to use the device under test.
     * Otherwise, use the name specified in the test.json for resource devices.
     */
    @Nullable
    String deviceName();
}
