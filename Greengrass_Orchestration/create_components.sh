#Create S3 Bucket
aws s3api create-bucket --bucket greengrass-orchestration --region $AWS_REGION --create-bucket-configuration LocationConstraint=$AWS_REGION

#Push all source to S3 Bucket
aws s3 sync . s3://greengrass-orchestration/

#Create Greengrass components for pushed S3 source
aws greengrassv2 create-component-version --cli-binary-format raw-in-base64-out --inline-recipe file://$PWD/Reboot/recipe/com.example.reboot-1.0.0.json
aws greengrassv2 create-component-version --cli-binary-format raw-in-base64-out --inline-recipe file://$PWD/DeviceMetrics/recipe/com.example.DevMetrics-1.0.0.json
aws greengrassv2 create-component-version --cli-binary-format raw-in-base64-out --inline-recipe file://$PWD/TrafficAnalysis/recipe/com.example.trafficAnalysis-1.0.0.json

#Create Greengrass components for ECR Docker images
aws greengrassv2 create-component-version --cli-binary-format raw-in-base64-out --inline-recipe file://$PWD/Video_capture_container/recipe/com.example.vidcap-1.0.0.json
aws greengrassv2 create-component-version --cli-binary-format raw-in-base64-out --inline-recipe file://$PWD/Application_container/recipe/com.example.appcnt-1.0.0.json
aws greengrassv2 create-component-version --cli-binary-format raw-in-base64-out --inline-recipe file://$PWD/ML_Inference_container/recipe/com.example.mlinf-1.0.0.json
aws greengrassv2 create-component-version --cli-binary-format raw-in-base64-out --inline-recipe file://$PWD/Cloud_container/recipe/com.example.cspaws-1.0.0.json

