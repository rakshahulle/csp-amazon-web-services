import time
import traceback
import json
import argparse
import boto3
import awsiot.greengrasscoreipc
import awsiot.greengrasscoreipc.client as client
from awsiot.greengrasscoreipc.model import (
    IoTCoreMessage, QOS, PublishToIoTCoreRequest, SubscribeToIoTCoreRequest)


ap = argparse.ArgumentParser()
ap.add_argument("-pt", "--pub_topic", default="device/stats",
                help="Publish topic for publishing the Analysis results.")
ap.add_argument("-st", "--sub_topic", default="device/anl_res",
                help="Topic name under which the Cloud contiainer pushes the detection statistics")
args = vars(ap.parse_args())

pub_topic = args["pub_topic"]
sub_topic = args["sub_topic"]

TIMEOUT = 1
qos = QOS.AT_LEAST_ONCE
subqos = QOS.AT_MOST_ONCE

ipc_client = awsiot.greengrasscoreipc.connect()


class SubHandler(client.SubscribeToIoTCoreStreamHandler):
    def __init__(self):
        super().__init__()

    def on_stream_event(self, event: IoTCoreMessage) -> None:
        try:
            message = str(event.message.payload, "utf-8")
            topic_name = event.message.topic_name
            # Handle message.
            jsonmsg = json.loads(message)
            # Analyze the metrics received and post analysis result
            obj = int(jsonmsg["total_count"])
            if obj > 10:
                payload = "High traffic detected...!!!"
            elif obj <= 10 and obj > 5:
                payload = "Medium traffic detected..!!"
            else:
                payload = "Low traffic detected.!"

            msg = json.dumps({"analysis_result": payload})

            pubrequest = PublishToIoTCoreRequest()
            pubrequest.topic_name = pub_topic
            pubrequest.payload = bytes(msg, "utf-8")
            pubrequest.qos = qos
            operation = ipc_client.new_publish_to_iot_core()
            operation.activate(pubrequest)
            future = operation.get_response()
            # future.result(TIMEOUT)

        except:
            pass

    def on_stream_error(self, error: Exception) -> bool:
        # Handle error.
        return True  # Return True to close stream, False to keep stream open.

    def on_stream_closed(self) -> None:
        # Handle close.
        pass


subrequest = SubscribeToIoTCoreRequest()
subrequest.topic_name = sub_topic
subrequest.qos = subqos
handler = SubHandler()
operation = ipc_client.new_subscribe_to_iot_core(handler)
future = operation.activate(subrequest)
future.result(TIMEOUT)

while True:
    pass
