import psutil
import json
import argparse
import time
from awsiot.greengrasscoreipc.model import (IoTCoreMessage, QOS, PublishToIoTCoreRequest, SubscribeToIoTCoreRequest)
import awsiot.greengrasscoreipc
import awsiot.greengrasscoreipc.client as client


# Initialize parser
ap = argparse.ArgumentParser()
ap.add_argument("-cw_n", "--cw_namespace", default = "DEFAULT", help="Cloudwatch namespace to push the device statistics")
ap.add_argument("-t", "--topic", default = "cloudwatch/metric/put", help="MQTT topic that cloudwatch component is subscribed to..")
ap.add_argument("-cw_i", "--cloudwatch_post_interval", default = 60, help="MQTT topic that cloudwatch component is subscribed to..")
args = vars(ap.parse_args())

TIMEOUT = 1
qos = QOS.AT_LEAST_ONCE
subqos = QOS.AT_MOST_ONCE
ipc_client = awsiot.greengrasscoreipc.connect()

cw_namespace = args["cw_namespace"]


def get_stats():
    mem_used = psutil.virtual_memory().used / (1024 * 1024)
    cpu_per = psutil.cpu_percent()
    return cpu_per,  int(mem_used)

while True:
    cpu_usage, mem_usage = get_stats()
    # print(cpu_usage, mem_usage)

    pub_msg_1 = {"request": {"namespace": cw_namespace, "metricData": {"metricName": "Device_status","value": 1,"unit": "None"}}}
    pub_msg_2 = {"request": {"namespace": cw_namespace, "metricData": {"metricName": "Cpu Usage","value": cpu_usage,"unit": "None"}}}
    pub_msg_3 = {"request": {"namespace": cw_namespace, "metricData": {"metricName": "Memory usage","value": mem_usage,"unit": "None"}}}


    dev_metr_req = PublishToIoTCoreRequest()
    dev_metr_req.topic_name = args["topic"]
    dev_metr_req.qos = qos
    dev_metr_operation = ipc_client.new_publish_to_iot_core()
    msg = json.dumps(pub_msg_1)
    dev_metr_req.payload = bytes(msg, "utf-8")
    dev_metr_operation.activate(dev_metr_req)
    future = dev_metr_operation.get_response()
    
    
    mem_usg_req = PublishToIoTCoreRequest()
    mem_usg_req.topic_name = args["topic"]
    mem_usg_req.qos = qos
    mem_usg_operation = ipc_client.new_publish_to_iot_core()
    msg = json.dumps(pub_msg_2)
    mem_usg_req.payload = bytes(msg, "utf-8")
    mem_usg_operation.activate(mem_usg_req)
    future = mem_usg_operation.get_response()
    
    
    cpu_usg_req = PublishToIoTCoreRequest()
    cpu_usg_req.topic_name = args["topic"]
    cpu_usg_req.qos = qos
    cpu_usg_operation = ipc_client.new_publish_to_iot_core()
    msg = json.dumps(pub_msg_3)
    cpu_usg_req.payload = bytes(msg, "utf-8")
    cpu_usg_operation.activate(cpu_usg_req)
    future = cpu_usg_operation.get_response()
    time.sleep(args["cloudwatch_post_interval"])

