import time
import traceback
import json
import boto3
import os
import awsiot.greengrasscoreipc
import awsiot.greengrasscoreipc.client as client
from awsiot.greengrasscoreipc.model import (IoTCoreMessage, QOS, PublishToIoTCoreRequest, SubscribeToIoTCoreRequest)

subscribetopic = "device/reboot"

TIMEOUT = 1
qos = QOS.AT_LEAST_ONCE
subqos = QOS.AT_MOST_ONCE

ipc_client = awsiot.greengrasscoreipc.connect()

class SubHandler(client.SubscribeToIoTCoreStreamHandler):
    def __init__(self):
        super().__init__()

    def on_stream_event(self, event: IoTCoreMessage) -> None:
        try:
            message = str(event.message.payload, "utf-8")
            topic_name = event.message.topic_name
            # Handle message.
            jsonmsg = json.loads(message)
            obj = jsonmsg["reboot"]
            print(obj)
            if obj=="True" or obj=="true":                                                                                                                             
                print("Rebooting the device")      
                # os.system("reboot") 
        except Exception as e:
            print("Error while processing....")

    def on_stream_error(self, error: Exception) -> bool:
        # Handle error.
        return True  # Return True to close stream, False to keep stream open.

    def on_stream_closed(self) -> None:
        # Handle close.
        pass

subrequest = SubscribeToIoTCoreRequest()
subrequest.topic_name = subscribetopic
subrequest.qos = subqos
handler = SubHandler()
operation = ipc_client.new_subscribe_to_iot_core(handler)
future = operation.activate(subrequest)
future.result(TIMEOUT)
#future.result()

while True:
    pass
