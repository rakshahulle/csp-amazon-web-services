import boto3

cloudwatch = boto3.client('cloudwatch', region_name='us-east-1')

# cloudwatch namespace name 
CLOUDWATCH_NAMESPACE = "cloudwatch_metrics"


# Publish metric data to CloudWatch
def cw(deviceId, metricValue, metricName):

    metric_data = {
        'MetricName': metricName,
        'Dimensions': [{'Name': deviceId + '_data', 'Value': deviceId}],
        'Unit': 'None', 
        'Value': metricValue
    }
    print(metric_data)
    
    cloudwatch.put_metric_data(MetricData=[metric_data],Namespace=CLOUDWATCH_NAMESPACE) 
    return


def lambda_handler(event, context):
    
    e = event['request']

    #extracting device name and metric data
    device_id = e['namespace']
    metricData = e['metricData']
    # print(metricData)
    
    #extracting metric name and its value
    metricName = metricData['metricName']
    value = metricData['value']
    
    #sending to cloudwatch matics
    cw(device_id,value, metricName)
        
    return 