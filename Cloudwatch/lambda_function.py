import json
import boto3

device = '<DEVICE_NAME>'
topic = device + "/reboot"

client = boto3.client('iot-data', region_name='<AWS_REGION>')


def lambda_handler(event, context):
    """
    AWS Lambda handler function to trigger a device reboot using AWS IoT.

    This function is triggered by an event and publishes a message to an IoT topic
    that corresponds to the specified device's reboot. This triggers the device to reboot.

    Args:
        event: The event data passed to the Lambda function.
        context: The context object passed to the Lambda function.

    Returns:
        A response string indicating the reboot action and a link to another Lambda function.
    """
    response = client.publish(
        topic=topic,
        qos=0,
        payload=json.dumps({"reboot": True})
    )
    return '''<center><a class="btn btn-primary">   DEVICE_NAME   </a><cwdb-action action="call" endpoint="<LAMBDA_FUNCTION_ARN>"></cwdb-action> </center>'''
