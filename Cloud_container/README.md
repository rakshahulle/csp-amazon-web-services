## Configuring AWS services
- For setting up the AWS IoT Advisor follow the document located at docs/ARM-Smart_camera_CSP_AWS_IoT_Advisor_setup.pdf.
- For setting up the AWS Kinesis Video Stream follow the document located at docs/ARM-Smart_camera_CSP_AWS_Kinesis_video_stream_setup.pdf.

## Building the Cloud container 
- Follow  the  steps given  below  for setting  up the container, 
- Navigate  to root  of project  folder.  The following  command  should  be  executed 
on the  target  device (ARM System Ready Device).  
```sh
 $ cd Cloud_container/ 
``` 
## Configuring Cloud container
- Open  the config  file located  at config/cloud_credential.json  in an  editor. 
- Update the Path for AWS IoT Certificate folder which was used to create Greengrass.
- Enter  the mqtt  publish  topic. This can be given as per user choice, for example device_1/det_stats. Note that this topic should be given while configuring recipes for components in further steps.
- Cloudwatch namespace can be given as per user choice, for example DEVICE1 can be namespace for device 1
- For IoT Endpoint, enter the data endpoint obtained while configuring greengrass. 
  - The user can also get this in AWS IoT Core console. 
  - Go to AWS IoT console  select  your region  and  on the left  side menu select  settings.  Under the Device  data  point  copy  the endpoint  URL and  update  it in config  file. 
- Save the config  file. 
- Open the kvs_credential.json in an editor 
- If user wants to use AWS cloud service provider, then open the config/ kvs_credential.json file in an editor and update the kvs configuration like accessKey, secretKey, region, and streamName. Use Stream Name which was created during Setting up Amazon Kinesis Video Streams on AWS Cloud and give region code where stream is created.
- Save the config file. 
- Copy the  device certificates  folder created while Greengrass configuration  to current  directory. 

```sh
 $ cp <PATH_TO_DEVICE_CERTIFICATES_FOLDER> .    
```

- Navigate from Cloud_container/ to src/ folder  
```sh
cd src/ 
```
- Open the run.py and go on to line 286 and in the Job Document paste the Object URL which you have noted before save that file. 
- Go to the config file of job 
```sh
cd jobs/ 
```
- Open the job.json file give the Iot Endpoint which is noted previously and save the file. 
- Run the command  given  below  for building  the cloud  container   
```sh
 # Mention the image name & tag name of your choice    
 $ docker build -t <IMAGE_NAME>:<TAG_NAME>.  
  
 # Check the Docker Images list with the command below    
 $ docker images  
```
## Deploy the Cloud container
- Deploy  the cloud  container  using the  command  below   
```sh
$ docker run --network=host $ docker run --network=host –it <IMAGE_NAME>:<TAG_NAME> -p <PORT>
-rek rek -adv <ADV_STATUS> -ts <TEST_SUITE_TO_BE_STARTED> -cw_i
<CLOUAWATCH_POST_INTERVAL>
 ```
- Required arguments: 
  - -p: Cloud container Flask server port number. for example 8040

 

- Optional arguments: 
  - -cw_i:	This is optional argument for posting a CloudWatch message in a time interval. The default value is 60 seconds.

  - -adv:	This is an optional argument which is given when we want to run device advisor, default value is off. Specify on to start the Advisor 

  - -ts:	This is optional argument specified when advisor is given on. This argument provides the Test suite to be started for Advisor. 
      - AWS qualification suite as aws 
      - Long duration test suite as ld 
      - Permission test suite as permission 
      - MQTT5 test suite as mqtt5  
      - MQTT3 and TLS test suite as custom  
      - Shadow test suite as shadow
  - -rek: This is optional argument, which is given when we want to run AWS
Rekognition, default value is off. Specify rek to start Rekognition
   - Note that to run application with AWS Rekognition enabled, Video capturing container, application
container and CSP container should be running, inference container is not
required.


A sample container running command would be like 
- With AWS Rekognition,
```sh
$ docker run --network=host –it csp_aws:v1 –p 8040 –rek rek
```
- With AWS Advisor running 
```sh
$ docker run --network=host –it csp_aws:v1 -p 8040 -adv on -ts aws 
```

  - Without AWS Advisor running 
    - This container will run the KVS pipeline also as default.
```sh
$ docker run --network=host –it csp_aws:v1 -p 8040
```
