# Use a base image of arm64v8/ubuntu:18.04
FROM arm64v8/ubuntu:18.04
# Use a base image of python:3.8-slim-buster
FROM python:3.8-slim-buster
# Set the DEBIAN_FRONTEND environment variable
ARG DEBIAN_FRONTEND=noninteractive

# Update package information
RUN apt-get update
# Install required libraries
RUN apt install -y libgl1-mesa-glx libglib2.0-0 libssl-dev
RUN apt-get clean -y
RUN apt-get install -y cmake

# Install additional libraries
RUN apt-get install -y libssl-dev 
RUN apt-get install -y libcurl4-openssl-dev 
RUN apt-get install -y liblog4cplus-dev 
RUN apt-get install -y libgstreamer1.0-dev 
RUN apt-get install -y libgstreamer-plugins-base1.0-dev
RUN apt-get install -y gstreamer1.0-plugins-base-apps
RUN apt-get install -y gstreamer1.0-plugins-bad
RUN apt-get install -y gstreamer1.0-plugins-good
RUN apt-get install -y gstreamer1.0-plugins-ugly
RUN apt-get install -y gstreamer1.0-tools

# Install git and ffmpeg
RUN apt-get install -y git
RUN apt-get install -y ffmpeg

# Upgrade pip
RUN python3 -m pip install --upgrade pip

# Install Python packages
RUN python3 -m pip install --no-cache-dir --upgrade pip
RUN python3 -m pip install --no-cache-dir numpy
RUN python3 -m pip install --no-cache-dir requests
RUN python3 -m pip install --no-cache-dir boto3
RUN python3 -m pip install --no-cache-dir flask
RUN python3 -m pip install --no-cache-dir flask_ngrok
RUN python3 -m pip install --no-cache-dir awsiot awsiotsdk

RUN apt-get install -y git nasm pkg-config libx264-dev libxext-dev libunistring-dev libaom-dev
RUN apt-get update && apt-get install --fix-missing -y build-essential libssl-dev libass-dev   libfreetype6-dev   libgnutls28-dev   libsdl2-dev   libtool   libva-dev   libvdpau-dev   libvorbis-dev   libxcb1-dev   libxcb-shm0-dev   libxcb-xfixes0-dev   meson   ninja-build   pkg-config   texinfo   wget   yasm   zlib1g-dev
RUN apt-get -y install cmake pkg-config libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev
RUN apt-get install libssl-dev libcurl4-openssl-dev liblog4cplus-dev gstreamer1.0-plugins-base-apps -y && apt-get clean -y
RUN git clone https://github.com/opencv/opencv.git
RUN \
        cd opencv && \
        git checkout 4.5.4 && \
        git submodule update --recursive --init && \
        mkdir build && \
        cd build && \
        cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
        -D WITH_GSTREAMER=ON \
        -D PYTHON3_EXECUTABLE=$(which python3) \
        -D PYTHON3_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
   	-D PYTHON3_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
        -D BUILD_EXAMPLES=ON -D WITH_FFMPEG=ON .. && \
        make -j$(nproc) && \
        make install && \
        ldconfig

WORKDIR /

# Clone Amazon Kinesis Video Streams Producer SDK C++
RUN git clone https://github.com/awslabs/amazon-kinesis-video-streams-producer-sdk-cpp.git
WORKDIR amazon-kinesis-video-streams-producer-sdk-cpp/
RUN mkdir build
WORKDIR /amazon-kinesis-video-streams-producer-sdk-cpp/build/
RUN cmake .. -DBUILD_GSTREAMER_PLUGIN=ON -DBUILD_JNI=FALSE -DBUILD_DEPENDENCIES=OFF
RUN make

# Set environment variables
WORKDIR /amazon-kinesis-video-streams-producer-sdk-cpp

ENV GST_PLUGIN_PATH=/amazon-kinesis-video-streams-producer-sdk-cpp/build
ENV LD_LIBRARY_PATH=/amazon-kinesis-video-streams-producer-sdk-cpp/open-source/local/lib

# Install build-essential and clone AWS IoT Device Client
WORKDIR /
RUN apt-get install build-essential -y
RUN git clone https://github.com/awslabs/aws-iot-device-client
RUN apt-get update && apt-get install -y cmake
RUN mkdir -p aws-iot-device-client/build
WORKDIR aws-iot-device-client/build
RUN cmake ../
RUN cmake --build . --target aws-iot-device-client
RUN ./aws-iot-device-client --help
COPY /jobs /jobs

# Set the working directory
WORKDIR /

# Copy source code and configuration files
COPY src/ src/
COPY config/ config/
COPY main.py main.py
COPY device_certs/ device_certs/

# Define the entry point for the container
ENTRYPOINT ["python3","main.py"]

