import json
import argparse
import time
import os
import shutil
import math
import boto3
import cv2
import subprocess
import numpy as np
from threading import Thread
from flask import Flask, request, redirect, flash, jsonify
from src.argconfig import ArgConf
from src.cloud_agent import AwsAgent
from src.logger import get_logger

flag = 0
kvs_frame = None

kvs_config_file = open("config/kvs_credential.json")
kvs_new_config = json.load(kvs_config_file)

ap = argparse.ArgumentParser()
args = ArgConf(ap)

app = Flask(__name__)

# Create logger for various debugging logs.
logger = get_logger()

# Initialize AWS Cloud Agent
cloud_agent = AwsAgent(args["cloudwatch_post_interval"], args["advisor"])

class ReadFromRTSP(Thread):
    def __init__(self, rtsp_src):
        Thread.__init__(self)
        self.stream = cv2.VideoCapture(rtsp_src)
        
        if not self.stream.isOpened():
            logger.error("Could not initialize the RTSP stream.....")
            exit()
        
        self.stream.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        self.status , self.frame = self.stream.read()
        while not self.status:
            self.status, self.frame = self.stream.read()
        self.stop = False
        self.frame_read_time = 0

    def stop(self):
        self.stop = True

    def run(self):
        while(self.stream.isOpened()):
            self.frame_read_start = time.time()
            self.status, self.frame = self.stream.read()
            if self.frame is None:
                continue
            frame_read_end = time.time()
            self.frame_read_time = frame_read_end - self.frame_read_start
            if (self.frame is None) or (self.stop == True):
                logger.info("Stream stopped / Connection broken.......")
                break
    def read(self):
        return self.status, self.frame
    def isOpened(self):
        return self.stream.isOpened()
    def get(self, id):
        return self.stream.get(id)


class RTSP_KVS(Thread):
    global kvs_new_config
    def __init__(self):
        Thread.__init__(self)
        self.gst_out = f"appsrc ! autovideoconvert ! x264enc bframes=0 key-int-max=45 bitrate=500 ! video/x-h264,stream-format=avc,alignment=au ! kvssink stream-name={kvs_new_config['streamName']} framerate=10 storage-size=512 access-key={kvs_new_config['accessKey']} secret-key={kvs_new_config['secretKey']} aws-region={kvs_new_config['region']}"
        self.out = cv2.VideoWriter(self.gst_out, cv2.CAP_GSTREAMER, 0, 10.0, (640, 480))
        if not self.out.isOpened():
            logger.info("Failed to open output")
            exit()
        
    def stop(self):
        self.stop = True

    def run(self):
        global kvs_frame
        previous_frame = None
        
        while True:
            if kvs_frame is None:
                logger.info("No new frame available, sending the previous frame")
                if previous_frame is not None:
                    self.out.write(previous_frame)
                time.sleep(1)
            else:
                self.out.write(kvs_frame)
                previous_frame = kvs_frame
                logger.info("Sent new frame to kvs")
                time.sleep(1/12)

if args["rekognition"]=="rek":
    os.environ['AWS_ACCESS_KEY_ID'] = kvs_new_config['accessKey']
    os.environ['AWS_SECRET_ACCESS_KEY'] = kvs_new_config['secretKey']
    kvs_thread = RTSP_KVS()
    kvs_thread.start()

person = 0
car = 0
truck = 0
bicycle = 0
bus = 0
end_to_end_fps = 0
pure_fps = 0 
flag = 0

@app.route('/video', methods=['POST'])
def text():
    """
    Handle incoming POST requests for video data.

    This function receives video data from an external source, processes it,
    and sends metrics to the AWS Cloud. It also handles launching a GStreamer pipeline
    based on a certain flag value.

    Returns:
        A JSON response indicating the status of processing and pipeline execution.
    """
    global flag, kvs_frame
    if request.method == 'POST':
        if flag == 0:
            kvs_thread = RTSP_KVS()
            kvs_thread.start()
            flag = 1
        logger.info(request.json['cloud_metrics'])
        img = np.asarray(bytearray(request.json["image"]), dtype='uint8')
        img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        img = cv2.resize(img,(640,480)) 
        kvs_frame = img
        cloud_agent.send_metrics_to_cloud(request.json['cloud_metrics'])
        end_rep = time.time()
        return jsonify({"message" : "Sent data to cloud", "End_to_End_pipeline_time": end_rep})

if __name__ == '__main__': 
    if args["rekognition"]=="rek":

        cloud_config_file = open("config/cloud_credential.json")
        cloud_new_config = json.load(cloud_config_file)

        reko_client = boto3.client('rekognition', region_name='ap-south-1')
        input_file = "rtsp://127.0.0.1:8554/cam"

        cap = ReadFromRTSP(input_file)
        cap.start()
        # cap = cv2.VideoCapture(input_file)
        frame_rate = cap.get(5)
        # Frame rate.

        width = cap.get(3)  # float `width`
        height = cap.get(4)

        while cap.isOpened():
            end_to_end_fps_start = time.time()
            frame_id = cap.get(1)  # Current frame number.
            ret, frame = cap.read()
            if ret is not True:
                break
            frame = cv2.resize(frame, (640, 480))

            has_frame, image_bytes = cv2.imencode(".jpg", frame)
            h, w = frame.shape[:2]

            if has_frame:
                pure_fps_start = time.time()
                response = reko_client.detect_labels(
                    Image={'Bytes': image_bytes.tobytes()})
                pure_fps_end = time.time()
                pure_fps = 1 / (pure_fps_end-pure_fps_start)

                logger.info(response)
                
                for label in response['Labels']:
                    if label['Name'] in ['Car', 'Bus', 'Person', 'Truck', 'Bicycle']:
                        label_text = label['Name']

                        if label_text == "Car":
                            color = (255, 0, 0)
                            car = len(label['Instances'])
                        elif label_text=="Person":
                            color =  (0, 255, 0)
                            person = len(label['Instances'])
                        elif label_text=="Bus":
                            color =  (0, 255, 255)
                            bus = len(label['Instances'])
                        elif label_text=="Bicycle":
                            color =  (255, 0, 127)
                            bicycle = len(label['Instances'])
                        else:
                            color = (255,127,127)
                            truck = len(label['Instances'])
                        for instance in label['Instances']:
                            # Extract bounding box coordinates
                            left = int(instance['BoundingBox']['Left'] * frame.shape[1])
                            top = int(instance['BoundingBox']['Top'] * frame.shape[0])
                            width = int(instance['BoundingBox']['Width'] * frame.shape[1])
                            height = int(instance['BoundingBox']['Height'] * frame.shape[0])
                            right = left + width
                            bottom = top + height
                        
                            # Draw the bounding box on the frame
                            cv2.rectangle(frame, (left, top), (right, bottom), color, 2)
                            
                            # Display the label and confidence
                            
                            cv2.putText(frame, label_text, (left, top - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
          
            kvs_frame =frame.copy() 
            end_to_end_fps_end = time.time()
            end_to_end_fps = 1 / (end_to_end_fps_end-end_to_end_fps_start)
            total_count = person+bicycle+car+bus+truck
            logger.info("bus %s", bus)
            logger.info("car %s", car)
            logger.info("truck %s", truck)
            logger.info("person %s", person)
            logger.info("Bicycle %s", bicycle)
            logger.info("total count %s", total_count)

            metrics = {"person":person, "bicycle": bicycle, "car":car,
                       "bus":bus, "truck":truck,
                       "End_to_end_FPS":end_to_end_fps, "Pure_inf":pure_fps, 
                       "total_count":total_count, "post_cloudwatch": "True"}

            data_str = json.dumps(metrics) 
            cloud_agent.publish_rek_metrics(cloud_new_config["pub_topic"],data_str)
            logger.info(frame_id)
   
        cap.release()
        cv2.destroyAllWindows()

    else:
        app.run(host="0.0.0.0",port=args['cloud_port'])
