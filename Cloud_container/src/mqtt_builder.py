import sys
import argparse
from awscrt import mqtt
from awscrt import io, http, auth
from awsiot import mqtt_connection_builder
from src.logger import get_logger


# Create logger for various debugging logs.
logger = get_logger()

class MQTT_builder:
    def __init__(self, description) -> None:
        self.parser = argparse.ArgumentParser(
            description="Send and receive messages through and MQTT connection.")
        self.commands = {}
        self.parsed_commands = None

    def build_mqtt_connection(self, on_connection_interrupted, on_connection_resumed, endpoint, port, cert_path, pri_key, ca_path, client_id):
        mqtt_connection = mqtt_connection_builder.mtls_from_path(
            endpoint=endpoint,
            port=port,
            cert_filepath=cert_path,
            pri_key_filepath=pri_key,
            ca_filepath=ca_path,
            on_connection_interrupted=on_connection_interrupted,
            on_connection_resumed=on_connection_resumed,
            client_id=client_id,
            clean_session=False,
            keep_alive_secs=30,
            http_proxy_options=None)
        return mqtt_connection


def on_connection_interrupted(connection, error, **kwargs):
    logger.warning("Connection interrupted. error: %s", error)


def on_connection_resumed(connection, return_code, session_present, **kwargs):
    logger.info("Connection resumed. return_code: %s session_present: %s", return_code, session_present)
    if return_code == mqtt.ConnectReturnCode.ACCEPTED and not session_present:
        logger.info("Session did not persist. Resubscribing to existing topics...")
        resubscribe_future, _ = connection.resubscribe_existing_topics()
        resubscribe_future.add_done_callback(on_resubscribe_complete)


def on_resubscribe_complete(resubscribe_future):
    resubscribe_results = resubscribe_future.result()
    logger.info("Resubscribe results: %s", resubscribe_results)
    for topic, qos in resubscribe_results['topics']:
        if qos is None:
            logger.error("Server rejected resubscribe to topic: %s", topic)
            sys.exit("Server rejected resubscribe to topic: {}".format(topic))
