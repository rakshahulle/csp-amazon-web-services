import sys
import argparse
import json
import random
from concurrent.futures import Future
from awscrt import mqtt
from awscrt import mqtt5, exceptions
from awsiot import mqtt_connection_builder, mqtt5_client_builder
from src.logger import get_logger


# Create logger for various debugging logs.
logger = get_logger()

future_stopped = Future()
future_connection_success = Future()
backoff = Future()


class MQTT5_builder:
    def __init__(self, description) -> None:
        self.parser = argparse.ArgumentParser(
            description="Send and receive messages through and MQTT connection.")
        self.commands = {}
        self.parsed_commands = None
        self.load_cloud_config()
        self.cert = self.advisor_config.get("certificatePath")
        self.privatekey = self.advisor_config.get("privateKeyPath")
        self.rootCA = self.advisor_config.get("rootCAPath")

    def load_cloud_config(self):
        try:
            with open("config/advisor_credential.json") as config_file:
                self.advisor_config = json.load(config_file)
        except FileNotFoundError:
            logger.error("Config file 'advisor_credential.json' not found.")
            sys.exit(1)

    def build_websocket_mqtt5_client(self,
                                     endpoint=None,
                                     on_publish_received=None,
                                     on_lifecycle_stopped=None,
                                     on_lifecycle_attempting_connect=None,
                                     on_lifecycle_connection_success=None,
                                     on_lifecycle_connection_failure=None,
                                     on_lifecycle_disconnection=None
                                     ):
        mqtt5_client = mqtt5_client_builder.websockets_with_default_aws_signing(
            endpoint=endpoint,
            region=region,
            credentials_provider=credentials_provider,
            http_proxy_options=None,
            ca_filepath=ca_path,
            on_publish_received=on_publish_received,
            on_lifecycle_stopped=on_lifecycle_stopped,
            on_lifecycle_attempting_connect=on_lifecycle_attempting_connect,
            on_lifecycle_connection_success=on_lifecycle_connection_success,
            on_lifecycle_connection_failure=on_lifecycle_connection_failure,
            on_lifecycle_disconnection=on_lifecycle_disconnection,
            client_id=client_id)
        return mqtt5_client

    def build_direct_mqtt5_client(self,
                                  endpoint=None,
                                  on_publish_received=None,
                                  on_lifecycle_stopped=None,
                                  on_lifecycle_attempting_connect=None,
                                  on_lifecycle_connection_success=None,
                                  on_lifecycle_connection_failure=None,
                                  on_lifecycle_disconnection=None):
        mqtt5_client = mqtt5_client_builder.mtls_from_path(
            endpoint=endpoint,
            port=8883,
            cert_filepath=self.cert,
            pri_key_filepath=self.privatekey,
            ca_filepath=self.rootCA,
            http_proxy_options=None,
            on_publish_received=on_publish_received,
            on_lifecycle_stopped=on_lifecycle_stopped,
            on_lifecycle_attempting_connect=on_lifecycle_attempting_connect,
            on_lifecycle_connection_success=on_lifecycle_connection_success,
            on_lifecycle_connection_failure=on_lifecycle_connection_failure,
            on_lifecycle_disconnection=on_lifecycle_disconnection,
            client_id="dev_adv")
        return mqtt5_client

    def build_mqtt5_client(self,
                           endpoint=None,
                           on_publish_received=None,
                           on_lifecycle_stopped=None,
                           on_lifecycle_attempting_connect=None,
                           on_lifecycle_connection_success=None,
                           on_lifecycle_connection_failure=None,
                           on_lifecycle_disconnection=None,
                           backoff_jitter=None,
                           jitter_backoff=None,
                           exponential_backoff=None):

        return self.build_direct_mqtt5_client(endpoint=endpoint, on_publish_received=on_publish_received,
                                              on_lifecycle_stopped=on_lifecycle_stopped,
                                              on_lifecycle_attempting_connect=on_lifecycle_attempting_connect,
                                              on_lifecycle_connection_success=on_lifecycle_connection_success,
                                              on_lifecycle_connection_failure=on_lifecycle_connection_failure,
                                              on_lifecycle_disconnection=on_lifecycle_disconnection)

# Callback when any publish is received
def on_publish_received(publish_packet_data):
    publish_packet = publish_packet_data.publish_packet
    assert isinstance(publish_packet, mqtt5.PublishPacket)
    logger.info("Received message from topic '%s': %s", publish_packet.topic, publish_packet.payload)
    global received_count
    global received_count
    received_count += 1
    if received_count == cmdUtils.get_command("count"):
        received_all_event.set()


# Callback for the lifecycle event Stopped
def on_lifecycle_stopped(lifecycle_stopped_data: mqtt5.LifecycleStoppedData):
    logger.info("Lifecycle Stopped")
    global future_stopped
    future_stopped.set_result(lifecycle_stopped_data)


# Callback for the lifecycle event Connection Success
def on_lifecycle_connection_success(lifecycle_connect_success_data: mqtt5.LifecycleConnectSuccessData):
    logger.info("Lifecycle Connection Success")
    global future_connection_success
    future_connection_success.set_result(lifecycle_connect_success_data)


# Callback for the lifecycle event Connection Failure
def on_lifecycle_connection_failure(lifecycle_connection_failure: mqtt5.LifecycleConnectFailureData):
    logger.info("Lifecycle Connection Failure")
    logger.error("Connection failed with exception: %s", lifecycle_connection_failure.exception)


# callback for jitter backoff
def backoff_jitter(retry_jitter_mode: mqtt5.ExponentialBackoffJitterMode):
    logger.info("client is waiting")
    global backoff
    backoff.set_result(ExponentialBackoffJitterMode)


def jitter_backoff(jitter):
    b = 1
    r = 2
    MAX_BACKOFF = 20
    exp_backoff = min(b*r ^ i, MAX_BACKOFF)
    jitter = random(0, exp_backoff)
    return jitter


def exponential_backoff(exp_backoff):
    b = 1
    r = 2
    MAX_BACKOFF = 20
    exp_backoff = min(b*r ^ i, MAX_BACKOFF)
    return exp_backoff
