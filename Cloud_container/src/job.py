import threading
import json
import time
from awscrt import io, mqtt, auth, http
from src.mqtt_builder import MQTT_builder, on_connection_interrupted, on_connection_resumed, on_resubscribe_complete
from src.logger import get_logger

received_count = 0
received_all_event = threading.Event()

# Create logger for various debugging logs.
logger = get_logger()

class JOB:
    def __init__(self):
        """Initialize the JOB class."""
        self.mqtt_builder = MQTT_builder(
            "Basic Connect - Make a MQTT connection.")
        self.load_cloud_config()
        self.endpoint = None
        self.rootCA = self.advisor_config["aws_iot_cert_folder"] + \
            "/AmazonRootCA1.pem"
        self.cert = self.advisor_config["aws_iot_cert_folder"] + \
            "/" + "certificate.pem.crt"
        self.privatekey = self.advisor_config["aws_iot_cert_folder"] + \
            "/" + "private.pem.key"

    def load_cloud_config(self):
        """Load AWS IoT device configuration from JSON file."""
        config_file = open("config/cloud_credential.json")
        self.advisor_config = json.load(config_file)

    def connect_to_cloud(self):
        """Establish connection to AWS IoT Core."""
        clientId = "dev_adv"
        port = 8883
        self.conn_handler = self.mqtt_builder.build_mqtt_connection(
            on_connection_interrupted,
            on_connection_resumed,
            self.endpoint,
            port,
            self.cert,
            self.privatekey,
            self.rootCA,
            clientId
        )

        while True:
            logger.info("Connecting to endpoint with client ID")
            try:
                connect_future = self.conn_handler.connect()
                connect_future.result()
                logger.info("Connected to AWS IoT as MQTT client!")
                break
            except Exception as e:
                logger.error("Connection attempt failed with error:", e)
                logger.info("Retrying...")
                time.sleep(10)

    def reboot_callback(self, topic, payload, dup, qos, retain, **kwargs):
        """Callback function to handle incoming commands."""
        payload_type = type(payload.decode("utf-8"))
        logger.info("Received payload of type %s", payload_type)
        command = str(payload.decode("utf-8")
                      ).strip("\n{}][").replace(" ", "").split(":")
        logger.info("Received command: %s", command)

    def job_subscribe(self, qos=mqtt.QoS.AT_LEAST_ONCE):
        """Subscribe to the job notification topic."""
        topic = "$aws/things/cloud_thing/jobs/notify-next"
        subscribe_future, packet_id = self.conn_handler.subscribe(
            topic=topic, qos=qos, callback=self.reboot_callback)
        subscribe_result = subscribe_future.result()

    def job_publish(self, qos=mqtt.QoS.AT_LEAST_ONCE):
        """Publish a message to the specified topic."""
        topic = "armcsp/pub"
        message = "payload"
        logger.info("Publishing message to topic '%s': %s", topic, message)
        message_json = json.dumps(message)
        self.conn_handler.publish(
            topic=topic, payload=message_json, qos=qos, retain=True)
