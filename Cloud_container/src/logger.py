"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import logging
import logging.config


def get_logger(file_path="config/logger.conf"):
    """
    Set up and configure a logger based on the provided file path.

    Args:
        file_path (str): Path to the logger configuration file.

    Returns:
        logger: Configured logger instance.
    """
    logging.config.fileConfig(file_path)
    # creating logger
    logger = logging.getLogger('CSP')
    return logger
