import boto3
import os
import time
import argparse
import json
from threading import Thread
import threading
from awscrt import io, mqtt, auth, http
from awsiot import mqtt_connection_builder
from src.mqtt_3 import MQTT
from src.permission import Permission
from src.shadow import Shadow
from src.mqtt_5 import MQTT_5
from src.job import JOB
from src.run import advisor
from src.argconfig import ArgConf
from src.mqtt_builder import MQTT_builder, on_connection_interrupted, on_connection_resumed, on_resubscribe_complete
from src.logger import get_logger

event = threading.Event()
ap = argparse.ArgumentParser()
args = ArgConf(ap)
Test_suite = args["DA_testsuite"]

# Create logger for various debugging logs.
logger = get_logger()

class AwsAgent:
    def __init__(self, cloudwatch_post_interval, advisor_on):
        """Initialize the AwsAgent class."""
        self.cloudwatch_post_interval = cloudwatch_post_interval
        self.start_time = time.time()
        self.load_cloud_config()
        self.connect_to_cloud()
        self.topic = self.new_config["pub_topic"]
        self.cloudwatch_namespace = self.new_config["cloudwatch_namespace"]
        self.cloudwatch_pub_topic = self.new_config["cloudwatch_pub_topic"]
        if advisor_on:
            advisor_thread = Thread(
                target=self.start_advisor_publishing, args=())
            advisor_thread.start()

    def load_cloud_config(self):
        """Load AWS IoT device configuration from JSON file."""
        config_file = open("config/cloud_credential.json")
        self.new_config = json.load(config_file)

    def connect_to_cloud(self):
        """Establish connection to AWS IoT Core."""
        rootCAPath = self.new_config["aws_iot_cert_folder"] + \
            "/AmazonRootCA1.pem"
        certificatePath = self.new_config["aws_iot_cert_folder"] + \
            "/" + "certificate.pem.crt"
        privateKeyPath = self.new_config["aws_iot_cert_folder"] + \
            "/" + "private.pem.key"
        host = self.new_config["iot_endpoint"]
        port = 8883
        clientId = "device_csp"
        self.conn_builder = MQTT_builder(
            "Basic Connect - Make a MQTT connection.")
        self.conn_handler = self.conn_builder.build_mqtt_connection(on_connection_interrupted,
                                                                    on_connection_resumed,
                                                                    host, port, certificatePath,
                                                                    privateKeyPath, rootCAPath, clientId)
        connect_future = self.conn_handler.connect()
        connect_future.result()
        logger.info("Connected to AWS IOT as MQTT client!")

    def send_metrics_to_cloud(self, metrics):
        """Send metrics to AWS IoT Core and CloudWatch."""
        if ((time.time() - self.start_time) > float(self.cloudwatch_post_interval)):
            logger.info("published to cloudwatch")
            person_req = json.dumps({"request": {"namespace": self.cloudwatch_namespace, "metricData": {
                                    "metricName": "Person", "value": metrics["person"], "unit": "None"}}})
            bus_req = json.dumps({"request": {"namespace": self.cloudwatch_namespace, "metricData": {
                                 "metricName": "Car", "value": metrics["car"], "unit": "None"}}})
            car_req = json.dumps({"request": {"namespace": self.cloudwatch_namespace, "metricData": {
                                 "metricName": "Bus", "value": metrics["bus"], "unit": "None"}}})
            bicycle_req = json.dumps({"request": {"namespace": self.cloudwatch_namespace, "metricData": {
                                     "metricName": "Bicycle", "value": metrics["bicycle"], "unit": "None"}}})
            truck_req = json.dumps({"request": {"namespace": self.cloudwatch_namespace, "metricData": {
                                   "metricName": "Truck", "value": metrics["truck"], "unit": "None"}}})
            end_to_end_fps_req = json.dumps({"request": {"namespace": self.cloudwatch_namespace, "metricData": {
                                            "metricName": "End_to_End_FPS", "value": metrics["end_to_end_fps"], "unit": "None"}}})
            pure_fps_req = json.dumps({"request": {"namespace": self.cloudwatch_namespace, "metricData": {
                                      "metricName": "Pure_FPS", "value": metrics["pure_fps"], "unit": "None"}}})

            self.conn_handler.publish(
                topic=self.cloudwatch_pub_topic, payload=person_req, qos=mqtt.QoS.AT_LEAST_ONCE)
            self.conn_handler.publish(
                topic=self.cloudwatch_pub_topic, payload=car_req, qos=mqtt.QoS.AT_LEAST_ONCE)
            self.conn_handler.publish(
                topic=self.cloudwatch_pub_topic, payload=bus_req, qos=mqtt.QoS.AT_LEAST_ONCE)
            self.conn_handler.publish(
                topic=self.cloudwatch_pub_topic, payload=bicycle_req, qos=mqtt.QoS.AT_LEAST_ONCE)
            self.conn_handler.publish(
                topic=self.cloudwatch_pub_topic, payload=truck_req, qos=mqtt.QoS.AT_LEAST_ONCE)
            self.conn_handler.publish(
                topic=self.cloudwatch_pub_topic, payload=end_to_end_fps_req, qos=mqtt.QoS.AT_LEAST_ONCE)
            self.conn_handler.publish(
                topic=self.cloudwatch_pub_topic, payload=pure_fps_req, qos=mqtt.QoS.AT_LEAST_ONCE)
            self.start_time = time.time()

        metrics = {"person": metrics["person"], "bicycle": metrics["bicycle"], "car": metrics["car"],
                   "bus": metrics["bus"], "truck": metrics["truck"],
                   "End_to_end_FPS": metrics["end_to_end_fps"], "Pure_inf": metrics["pure_fps"],
                   "total_count": metrics["total_count"], "post_cloudwatch": "False"}

        data_str = json.dumps(metrics)
        self.conn_handler.publish(
            topic=self.topic, payload=data_str, qos=mqtt.QoS.AT_LEAST_ONCE)
            
    def publish_rek_metrics(self,topic,payload):
        self.conn_handler.publish(topic=topic,payload=payload,qos=mqtt.QoS.AT_LEAST_ONCE)
            
    def start_advisor_publishing(self):
        """Start the advisor test suite based on the specified Test_suite."""
        advisor_object = advisor()
        if Test_suite == "aws":
            advisor_object.create_thing()
            advisor_object.create_policy()
            advisor_object.aws_test_suite()
            advisor_object.run_aws_suite()
            advisor_object.delete_thing()
        elif Test_suite == "ld":
            advisor_object.create_thing()
            advisor_object.create_policy()
            advisor_object.long_duration_suite()
            advisor_object.run_aws_suite()
            advisor_object.delete_thing()
        elif Test_suite == "custom":
            advisor_object.create_thing()
            advisor_object.create_policy()
            advisor_object.create_test_suite()
            advisor_object.run_aws_suite()
            advisor_object.delete_thing()
        elif Test_suite == "mqtt5":
            advisor_object.create_thing()
            advisor_object.create_policy()
            advisor_object.Mqtt5_test_suite()
            advisor_object.run_mqtt5_suite()
            advisor_object.delete_thing()
        elif Test_suite == "permission":
            advisor_object.create_permission_thing()
            advisor_object.create_permission_policy()
            advisor_object.permission_test()
            advisor_object.run_p_suite()
            advisor_object.delete_p_thing()
        elif Test_suite == "shadow":
            advisor_object.create_thing()
            advisor_object.create_policy()
            advisor_object.Shadow_test()
            advisor_object.run_shadow_suite()
            advisor_object.delete_thing()
        elif Test_suite == "job":
            advisor_object.Job_suite()
            advisor_object.run_suite()
