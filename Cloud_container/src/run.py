import boto3
import os
import time
import json
from src.mqtt_3 import MQTT
from src.permission import Permission
from src.shadow import Shadow
from src.mqtt_5 import MQTT_5
from src.job import JOB
from src.logger import get_logger


# Create logger for various debugging logs.
logger = get_logger()


class advisor:
    def __init__(self):
        """Initialize the advisor class."""
        self.load_aws_config()
        self.region_name = self.aws_config["region_name"]
        self.access_id = self.aws_config["Access key ID"]
        self.secrete_id = self.aws_config["Secret access key"]
        self.device_role = self.aws_config["device_role"]
        self.thing_name = "Device_advisor_thing"
        self.policyname = "DA_policy"
        self.p_policyname = "DA_permission"
        self.p_thingname = "DA_permission_thing"
        self.iot_client = boto3.client('iot', region_name=self.region_name,
                                       aws_access_key_id=self.access_id, aws_secret_access_key=self.secrete_id)
        self.da_client = boto3.client('iotdeviceadvisor', region_name=self.region_name,
                                      aws_access_key_id=self.access_id, aws_secret_access_key=self.secrete_id)
        self.iot_da_client = boto3.client('iot-data', region_name=self.region_name,
                                          aws_access_key_id=self.access_id, aws_secret_access_key=self.secrete_id)

    def load_aws_config(self):
        """Load AWS IoT device configuration from JSON file."""
        config_file = open("config/aws_credential.json")
        self.aws_config = json.load(config_file)

    def endpoint(self):
        """Retrieve and return the Device Advisor endpoint address."""
        self.device_advisor_endpoint = self.iot_client.describe_endpoint(
            endpointType='iot:DeviceAdvisor')['endpointAddress']
        return self.device_advisor_endpoint

    def delete_thing(self):
        """Delete an IoT Thing, its certificate, and associated resources."""
        self.certificate_arn = self.create_cert_response['certificateArn']
        self.certificate_id = self.create_cert_response['certificateId']
        self.iot_client.detach_thing_principal(
            thingName=self.thing_name, principal=self.certificate_arn)
        self.iot_client.update_certificate(
            certificateId=self.certificate_id, newStatus='INACTIVE')
        self.iot_client.delete_certificate(
            certificateId=self.certificate_id, forceDelete=True)
        self.iot_da_client.delete_thing_shadow(thingName=self.thing_name)
        self.iot_client.delete_thing(thingName=self.thing_name)
        logger.info("Info: Thing is deleted successfully")

    def delete_p_thing(self):
        """Delete a permission-based IoT Thing, its certificate, and associated resources."""
        self.p_certificate_arn = self.create_p_cert_response['certificateArn']
        self.p_certificate_id = self.create_p_cert_response['certificateId']
        self.iot_client.detach_thing_principal(
            thingName=self.p_thingname, principal=self.p_certificate_arn)
        self.iot_client.update_certificate(
            certificateId=self.p_certificate_id, newStatus='INACTIVE')
        self.iot_client.delete_certificate(
            certificateId=self.p_certificate_id, forceDelete=True)
        self.iot_client.delete_thing(thingName=self.p_thingname)
        logger.info("Info: Thing is deleted successfully")

    def create_thing(self):
        """Create an IoT Thing, generate a certificate, and perform necessary attachments."""
        self.create_thing_name = self.iot_client.create_thing(
            thingName=self.thing_name)
        logger.info("IOT thing is created ... %s", self.create_thing_name['thingName'])
        self.thing_arn = self.create_thing_name['thingArn']
        self.create_cert_response = self.iot_client.create_keys_and_certificate(
            setAsActive=True)
        # create an temporary certificate/key file path
        self.certificate_path = os.path.join(
            os.getcwd(), 'certificate.pem.crt')
        self.key_path = os.path.join(os.getcwd(), 'private.pem.key')

        f = open(self.certificate_path, "w")
        f.write(self.create_cert_response['certificatePem'])
        f.close()

        f = open(self.key_path, "w")
        f.write(self.create_cert_response['keyPair']['PrivateKey'])
        f.close()
        self.certificate_arn = self.create_cert_response['certificateArn']
        self.certificate_id = self.create_cert_response['certificateId']

        self.iot_client.attach_thing_principal(
            thingName=self.create_thing_name['thingName'], principal=self.certificate_arn)
        logger.info("Attached certificate to test thing...")

        self.payload_shadow = {
            "state": {
                "reported": {
                    "online": "false"
                },
                "desired": {
                    "online": "false"
                }}}
        self.shadow_document = json.dumps(self.payload_shadow)
        self.shadow_response = self.iot_da_client.update_thing_shadow(
            thingName=self.thing_name, payload=self.shadow_document)
        self.get_shadow_response = self.iot_da_client.get_thing_shadow(
            thingName=self.thing_name)

    def create_policy(self):
        """Create a policy and attach it to an IoT Thing's certificate."""
        try:
            self.policy = {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Action": "*",
                        "Resource": "*"
                    }]}
            self.policy_document = json.dumps(self.policy)
            self.create_policy = self.iot_client.create_policy(policyName=self.policyname,
                                                               policyDocument=self.policy_document)
            logger.info("Policy is created ... %s", self.create_policy['policyName'])
            self.iot_client.attach_policy(
                policyName=self.create_policy['policyName'], target=self.certificate_arn)
            logger.info("Attached policy to test thing...")
        except:
            logger.info("policy already created .......")
            self.iot_client.attach_policy(
                policyName=self.policyname, target=self.certificate_arn)
            logger.info("Attached policy to test thing...")
            pass

    def create_permission_thing(self):
        """Create a permission-based IoT Thing, generate a certificate, and perform necessary attachments."""
        self.create_p_thing_name = self.iot_client.create_thing(
            thingName=self.p_thingname)
        logger.info("IOT thing is created ... %s", self.create_p_thing_name['thingName'])
        self.p_thing_arn = self.create_p_thing_name['thingArn']
        self.create_p_cert_response = self.iot_client.create_keys_and_certificate(
            setAsActive=True)
        # create an temporary certificate/key file path
        self.certificate_path = os.path.join(
            os.getcwd(), 'p_certificate.pem.crt')
        self.key_path = os.path.join(os.getcwd(), 'p_private.pem.key')

        f = open(self.certificate_path, "w")
        f.write(self.create_p_cert_response['certificatePem'])
        f.close()

        f = open(self.key_path, "w")
        f.write(self.create_p_cert_response['keyPair']['PrivateKey'])
        f.close()
        self.p_certificate_arn = self.create_p_cert_response['certificateArn']
        self.p_certificate_id = self.create_p_cert_response['certificateId']

        self.iot_client.attach_thing_principal(
            thingName=self.create_p_thing_name['thingName'], principal=self.p_certificate_arn)
        logger.info("Attached certificate to test thing...")

    def create_permission_policy(self):
        """Create a permission-based policy and attach it to an IoT Thing's certificate."""
        try:
            self.p_policy = {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Action": [
                            "iot:Publish",
                            "iot:Receive"
                        ],
                        "Resource": "arn:aws:iot:region:account:topic/test/topic"
                    },
                    {
                        "Effect": "Allow",
                        "Action": "iot:Subscribe",
                        "Resource": "arn:aws:iot:region:account:topicfilter/test/topic"
                    },
                    {
                        "Effect": "Allow",
                        "Action": "iot:Connect",
                        "Resource": "arn:aws:iot:region:account:client/test/topic"
                    }
                ]
            }
            self.p_policy_document = json.dumps(self.p_policy)
            self.create_p_policy = self.iot_client.create_policy(policyName=self.p_policyname,
                                                                 policyDocument=self.p_policy_document)
            logger.info("Policy is created ... %s", self.create_p_policy['policyName'])
            self.iot_client.attach_policy(
                policyName=self.p_policyname, target=self.p_certificate_arn)
            logger.info("Attached policy to test thing...")
        except:
            self.iot_client.attach_policy(
                policyName=self.p_policyname, target=self.p_certificate_arn)
            logger.info("policy already created .......")
            pass

    def create_test_suite(self):
        """
        Creates a test suite using MQTT test configurations and AWS IoT Device Advisor APIs.
        """
        self.mqtt3 = "{\"configuration\":{},\"tests\":[{\"name\":\"Custom_test_Group\",\"configuration\":{}, \"tests\":[{\"name\":\"MQTT_Connect_Happy_Case\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Connect\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT_Publish\",\"configuration\":{\"EXECUTION_TIMEOUT\":300,\"TOPIC_FOR_PUBLISH_VALIDATION\":\"armcsp/pub\"},\"test\":{\"id\":\"MQTT_Publish\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT_publish_retained_messages\",\"configuration\":{\"EXECUTION_TIMEOUT\":300,\"TOPIC_FOR_PUBLISH_RETAINED_VALIDATION\": \"armcsp/pub\"},\"test\":{\"id\":\"MQTT_Publish_Retained_Messages\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT_publish_retry_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300,\"TOPIC_FOR_PUBLISH_VALIDATION\": \"armcsp/pub\"},\"test\":{\"id\":\"MQTT_Publish_Retry_No_Puback\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT_subscribe\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Subscribe\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT_subscribe_retry_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Subscribe_Retry_No_Suback\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_connect_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Connect\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_Receive_Maximum_Size_Fragments\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Receive_Maximum_Size_Fragments\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_support_aws_iot_cipher_suites_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Support_AWS_IoT_Cipher_Suites\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_large_size_server_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Large_Size_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_unsecure_server_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Unsecure_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_incorrect_subject_name_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Incorrect_Subject_Name_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_expired_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Expired_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"Expired_persistent_session_test\",\"configuration\":{\"TRIGGER_TOPIC\": \"armcsp/sub\",\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Expired_Persistent_Session\",\"version\":\"0.0.0\"}},{\"name\":\"Persistent_session_happy_case\",\"configuration\":{\"TRIGGER_TOPIC\": \"armcsp/sub\",\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Persistent_Session_Happy_Case\",\"version\":\"0.0.0\"}},{\"name\":\"Mqtt No Ack PingResp\",\"configuration\":{\"EXECUTION_TIMEOUT\":306},\"test\":{\"id\":\"MQTT_No_Ack_PingResp\",\"version\":\"0.0.0\"}}]}]}"
        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'Custom_suite', 'devices': [
            {
                'thingArn': self.thing_arn
            },
        ],
            'intendedForQualification': False,
            'isLongDurationTest': False,
            'rootGroup': self.mqtt3,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV3_1_1'})
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("MQTT3 test suite is created")

    def aws_test_suite(self):
        """
        Creates a test suite for AWS qualification using MQTT and AWS IoT Device Advisor APIs.
        """
        self.aws = "{\"configuration\":{},\"tests\":[{\"name\":\"Qualification Program\",\"configuration\":{}, \"tests\":[{\"name\":\"MQTT_Connect\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Connect\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT_subscribe\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Subscribe\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT_Publish\",\"configuration\":{\"EXECUTION_TIMEOUT\":300,\"TOPIC_FOR_PUBLISH_VALIDATION\":\"armcsp/pub\"},\"test\":{\"id\":\"MQTT_Publish\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_connect\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Connect\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_Unsecure_Server_Cert\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Unsecure_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_incorrect_subject_name_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Incorrect_Subject_Name_Server_Cert\",\"version\":\"0.0.0\"}}]}]}"
        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'AWS_Qualification_suite', 'devices': [
            {
                'thingArn': self.thing_arn
            },
        ],
            'intendedForQualification': True,
            'isLongDurationTest': False,
            'rootGroup': self.aws,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV3_1_1'})
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("AWS qualification test suite is created")

    def long_duration_suite(self):
        """
        Creates a long duration test suite using MQTT and AWS IoT Device Advisor APIs.
        """
        self.LD = "{\"configuration\":{},\"tests\":[{\"name\":\"Long_duration\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"tests\":[{\"name\":\"MQTT_Long_duration_test\",\"configuration\":{\"OPERATIONS\": [\"PUBLISH\", \"SUBSCRIBE\"], \"SCENARIOS\": [\"LONG_SERVER_DISCONNECT\",\"RECONNECT_BACKOFF\",\"KEEP_ALIVE\",\"RECEIVE_LARGE_PAYLOAD\",\"INTERMITTENT_CONNECTIVITY\",\"PERSISTENT_SESSION\",\"PUBACK_QOS_1\"]},\"test\":{\"id\":\"MQTT_Long_Duration\",\"testCase\":null,\"version\":\"0.0.0\"}}]}]}"
        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'Long_duration_suite', 'devices': [
            {
                'thingArn': self.thing_arn
            },
        ],
            'intendedForQualification': False,
            'isLongDurationTest': True,
            'rootGroup': self.LD,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV3_1_1'})
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("Long duration test suite is created")

    def Mqtt5_test_suite(self):
        """
        Creates a test suite for MQTT version 5 using MQTT and AWS IoT Device Advisor APIs.
        """
        self.mqtt5 = "{\"configuration\":{},\"tests\":[{\"name\":\"MQTT_5_test_group\",\"configuration\":{}, \"tests\":[{\"name\":\"MQTT Connect Jitter Retries\",\"configuration\":{\"EXECUTION_TIMEOUT\":1200},\"test\":{\"id\":\"MQTT_Connect_Jitter_Backoff_Retries\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT Reconnect Backoff Retries On Server Disconnect\",\"configuration\":{\"EXECUTION_TIMEOUT\":900},\"test\":{\"id\":\"MQTT_Reconnect_Backoff_Retries_On_Server_Disconnect\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT Reconnect Backoff Retries On Unstable Connection\",\"configuration\":{\"EXECUTION_TIMEOUT\":900},\"test\":{\"id\":\"MQTT_Reconnect_Backoff_Retries_On_Unstable_Connection\",\"version\":\"0.0.0\"}},{\"name\":\"MQTT Connect Exponential Backoff Retries\",\"configuration\":{\"EXECUTION_TIMEOUT\":900},\"test\":{\"id\":\"MQTT_Connect_Exponential_Backoff_Retries\",\"version\":\"0.0.0\"}}]}]}"
        self.mqtt1 = "{\"configuration\":{},\"tests\":[{\"name\":\"MQTT_5_test_group\",\"configuration\":{}, \"tests\":[{\"name\":\"mqtt_user_property_test\",\"test\":{\"USER_PROPERTIES\": [{\"name\": \"name1\", \"value\":\"value1\"},{\"name\": \"name2\", \"value\":\"value2\"}],\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"MQTT_Publish_User_Property\",\"version\":\"0.0.0\"}}]}]}"
        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'MQTT5_suite', 'devices': [
            {
                'thingArn': self.thing_arn
            },
        ],
            'intendedForQualification': False,
            'isLongDurationTest': False,
            'rootGroup': self.mqtt5,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV5'})
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("Mqtt5 test suite is created")

    def permission_test(self):
        """
        Creates a test suite for testing security device policies using MQTT and AWS IoT Device Advisor APIs.
        """
        self.permission = "{\"configuration\":{},\"tests\":[{\"name\":\"Permission_test\",\"configuration\":{}, \"tests\":[{\"name\":\"Security_device_policies\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"Security_Device_Policies\",\"version\":\"0.0.0\"}}]}]}"
        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'Permission_test', 'devices': [
            {
                'thingArn': self.p_thing_arn
            },
        ],
            'intendedForQualification': False,
            'isLongDurationTest': False,
            'rootGroup': self.permission,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV3_1_1'})
        self.device_advisor_endpoint = self.iot_client.describe_endpoint(
            endpointType='iot:DeviceAdvisor')['endpointAddress']
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("Permission test suite is created")

    def Shadow_test(self):
        """
        Creates a test suite for testing Shadow functionality using MQTT and AWS IoT Device Advisor APIs.
        """
        self.shadow = "{\"configuration\":{},\"tests\":[{\"name\":\"Shadow test case\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"tests\":[{\"name\":\"Shadow_update_reported_state\",\"configuration\":{\"DESIRED_STATE\": {\"online\": \"true\"},\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"Shadow_Update_Reported_State\",\"version\":\"0.0.0\"}},{\"name\":\"Shadow_publish_reported_state\",\"configuration\":{\"REPORTED_STATE\": {\"online\": \"true\"}},\"test\":{\"id\":\"Shadow_Publish_Reported_State\",\"version\":\"0.0.0\"}}]}]}"

        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'Shadow_test', 'devices': [
            {
                'thingArn': self.thing_arn
            },
        ],
            'intendedForQualification': False,
            'isLongDurationTest': False,
            'rootGroup': self.shadow,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV3_1_1'})
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("Shadow test suite is created")

    def TLS_suite(self):
        """
        Creates a test suite for testing TLS using MQTT and AWS IoT Device Advisor APIs.
        """
        self.TLS = "{\"configuration\":{},\"tests\":[{\"name\":\"TLS_Group\",\"configuration\":{}, \"tests\":[{\"name\":\"TLS_connect_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Connect\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_Receive_Maximum_Size_Fragments\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Receive_Maximum_Size_Fragments\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_support_aws_iot_cipher_suites_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Support_AWS_IoT_Cipher_Suites\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_large_size_server_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Large_Size_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_unsecure_server_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Unsecure_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_incorrect_subject_name_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Incorrect_Subject_Name_Server_Cert\",\"version\":\"0.0.0\"}},{\"name\":\"TLS_expired_cert_test\",\"configuration\":{\"EXECUTION_TIMEOUT\":300},\"test\":{\"id\":\"TLS_Expired_Server_Cert\",\"version\":\"0.0.0\"}}]}]}"
        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'TLS suite', 'devices': [
            {
                'thingArn': self.thing_arn
            },
        ],
            'intendedForQualification': False,
            'isLongDurationTest': False,
            'rootGroup': self.TLS,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV3_1_1'})
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("TLS test suite is created")

    def Job_suite(self):
        """
        Creates a test suite for testing job execution using MQTT and AWS IoT Device Advisor APIs.
        """
        self.job_response = self.iot_client.describe_thing(
            thingName='cloud_thing')
        self.jobthing_arn = self.job_response['thingArn']
        self.job = "{\"configuration\":{},\"tests\":[{\"name\":\"Jobs\",\"configuration\":{},\"tests\":[{\"name\":\"Job_execution\",\"configuration\": {\"JOB_DOCUMENT_SOURCE\": \"https://advisorjob.s3.amazonaws.com/hello.json\",\"EXECUTION_TIMEOUT\":300},\"test\": {\"id\": \"Job_Execution\",\"version\": \"0.0.0\"}}]}]}"
        self.new_suite = self.da_client.create_suite_definition(suiteDefinitionConfiguration={'suiteDefinitionName': 'Job_suite', 'devices': [
            {
                'thingArn': self.jobthing_arn
            },
        ],
            'intendedForQualification': False,
            'isLongDurationTest': False,
            'rootGroup': self.job,
            'devicePermissionRoleArn': self.device_role,
            'protocol': 'MqttV3_1_1'})
        self.tsid = self.new_suite['suiteDefinitionId']
        logger.info("Job test suite is created")

    def run_suite(self):
        """
        Runs the specified test suite and handles the test execution status.
        """
        logger.info("start running the suite")
        try:
            self.start_response = self.da_client.start_suite_run(
                suiteDefinitionId=self.tsid,
                suiteRunConfiguration={
                    'primaryDevice': {
                        'thingArn': self.jobthing_arn
                    },
                    'parallelRun': False
                })
            self.tsrid = self.start_response['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)
        except Exception as e:
            # proceed with existing test suite run and update the test _suite_run_id
            logger.info("Test suite is already running")
            # get the existing run id
            self.definition_response = self.da_client.get_suite_definition(
                suiteDefinitionId=self.tsid
            )
            # print definition response
            self.response = self.da_client.list_suite_runs(
                suiteDefinitionId=self.tsid,
                suiteDefinitionVersion=self.definition_response['suiteDefinitionVersion'])
            self.tsrid = None
            for r in self.response['suiteRunsList']:
                if r['suiteDefinitionId'] == self.tsid and r['status'] == "RUNNING":
                    self.tsrid = r['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)

        self.tsr = self.da_client.get_suite_run(
            suiteDefinitionId=self.tsid,
            suiteRunId=self.tsrid)
        if self.tsr['status'] == 'FAIL' or self.tsr['status'] == 'PASS' or self.tsr['status'] == 'CANCELED':
            pass
        if self.tsr['status'] == 'PENDING':
            logger.info(" waiting for the test suite to be ready for connections...")
            self.interval = 5  # seconds
            sum = 0  # total time waiting
            while self.tsr['status'] == 'PENDING':
                logger.info(f"{self.tsr['status']} ({sum} seconds)")                
                time.sleep(self.interval)
                sum += self.interval
                self.tsr = self.da_client.get_suite_run(
                    suiteDefinitionId=self.tsid,
                    suiteRunId=self.tsrid)
        logger.info("Test suite status is " + self.tsr["status"])

        self.still_working = True
        while self.still_working:
            # get the list of test cases in the test suite
            self.run_status = self.da_client.get_suite_run(
                suiteDefinitionId=self.tsid, suiteRunId=self.tsrid)
            self.test_cases = self.run_status['testResult']['groups'][0]['tests']

            # build the test case dictionary containing Name:Status
            test_case_dict = {}
            for t in self.test_cases:
                test_case_dict[t["testCaseDefinitionName"]] = t['status']
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    self.still_working = False
                else:
                    self.still_working = True
            # run each test case in test suite
            for t in self.test_cases:
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    s = t['testCaseDefinitionName']
                    logger.info('%s%s', '{0:.<60}'.format(s), t['status'])
                elif t['status'] == "RUNNING":
                    s = t['testCaseDefinitionName']
                    logger.info('%s%s', '{0:.<60}'.format(s), t['status'])
                    logger.info("starting the pubsub test client ....")
                    test_job = JOB()
                    test_job.endpoint = self.endpoint()
                    test_job.connect_to_cloud()
                    test_job.job_subscribe()
                    test_job.job_publish()
                else:
                    break
                    logger.warning("unknown test case status! --> %s", t['status'])
                break
            time.sleep(1)
        logger.info("completed")

    def run_shadow_suite(self):
        """
        Runs the specified Shadow test suite and handles the test execution status.
        """
        logger.info("start running the suite")
        try:
            self.start_response = self.da_client.start_suite_run(
                suiteDefinitionId=self.tsid,
                suiteRunConfiguration={
                    'primaryDevice': {
                        'thingArn': self.thing_arn
                    },
                    'parallelRun': False
                })
            self.tsrid = self.start_response['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)
        except Exception as e:
            # proceed with existing test suite run and update the test _suite_run_id
            logger.info("Test suite is already running")
            # get the existing run id
            self.definition_response = self.da_client.get_suite_definition(
                suiteDefinitionId=self.tsid
            )
            # print definition response
            self.response = self.da_client.list_suite_runs(
                suiteDefinitionId=self.tsid,
                suiteDefinitionVersion=self.definition_response['suiteDefinitionVersion'])
            self.tsrid = None
            for r in self.response['suiteRunsList']:
                if r['suiteDefinitionId'] == self.tsid and r['status'] == "RUNNING":
                    self.tsrid = r['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)

        self.tsr = self.da_client.get_suite_run(
            suiteDefinitionId=self.tsid,
            suiteRunId=self.tsrid)
        if self.tsr['status'] == 'FAIL' or self.tsr['status'] == 'PASS' or self.tsr['status'] == 'CANCELED':
            pass
        if self.tsr['status'] == 'PENDING':
            logger.info(" waiting for the test suite to be ready for connections...")
            self.interval = 5  # seconds
            sum = 0  # total time waiting
            while self.tsr['status'] == 'PENDING':
                logger.info(f"{self.tsr['status']} ({sum} seconds)")
                time.sleep(self.interval)
                sum += self.interval
                self.tsr = self.da_client.get_suite_run(
                    suiteDefinitionId=self.tsid,
                    suiteRunId=self.tsrid)
        logger.info("Test suite status is " + self.tsr["status"])


        self.still_working = True
        while self.still_working:
            # get the list of test cases in the test suite
            self.run_status = self.da_client.get_suite_run(
                suiteDefinitionId=self.tsid, suiteRunId=self.tsrid)
            self.test_cases = self.run_status['testResult']['groups'][0]['tests']

            # build the test case dictionary containing Name:Status
            test_case_dict = {}
            for t in self.test_cases:
                test_case_dict[t["testCaseDefinitionName"]] = t['status']
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    self.still_working = False
                else:
                    self.still_working = True
            # run each test case in test suite
            for t in self.test_cases:
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    s = t['testCaseDefinitionName']
                elif t['status'] == "RUNNING":
                    s = t['testCaseDefinitionName']
                    logger.info('%s%s', '{0:.<60}'.format(s), t['status'])
                    logger.info("starting the pubsub test client ....")
                    test_shadow = Shadow()
                    test_shadow.endpoint = self.endpoint()
                    test_shadow.connect_to_cloud()
                    test_shadow.shadow_subscribe()
                    test_shadow.shadow_publish()
                else:
                    break
                    logger.warning("unknown test case status! --> %s", t['status'])
                break
            time.sleep(1)
        logger.info("completed")

    def run_aws_suite(self):
        """ Runs the specified AWS qualification test suite and handles the test execution status."""
        logger.info("start running the suite")
        try:
            self.start_response = self.da_client.start_suite_run(
                suiteDefinitionId=self.tsid,
                suiteRunConfiguration={
                    'primaryDevice': {
                        'thingArn': self.thing_arn
                    },
                    'parallelRun': False
                })
            self.tsrid = self.start_response['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)
        except Exception as e:
            # proceed with existing test suite run and update the test _suite_run_id
            logger.info("Test suite is already running")
            # get the existing run id
            self.definition_response = self.da_client.get_suite_definition(
                suiteDefinitionId=self.tsid
            )
            # print definition response
            self.response = self.da_client.list_suite_runs(
                suiteDefinitionId=self.tsid,
                suiteDefinitionVersion=self.definition_response['suiteDefinitionVersion'])
            self.tsrid = None
            for r in self.response['suiteRunsList']:
                if r['suiteDefinitionId'] == self.tsid and r['status'] == "RUNNING":
                    self.tsrid = r['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)

        self.tsr = self.da_client.get_suite_run(
            suiteDefinitionId=self.tsid,
            suiteRunId=self.tsrid)
        if self.tsr['status'] == 'FAIL' or self.tsr['status'] == 'PASS' or self.tsr['status'] == 'CANCELED':
            pass
        if self.tsr['status'] == 'PENDING':
            logger.info(" waiting for the test suite to be ready for connections...")
            self.interval = 5  # seconds
            sum = 0  # total time waiting
            while self.tsr['status'] == 'PENDING':
                logger.info('%s (%s seconds)', self.tsr['status'], str(sum))
                time.sleep(self.interval)
                sum += self.interval
                self.tsr = self.da_client.get_suite_run(
                    suiteDefinitionId=self.tsid,
                    suiteRunId=self.tsrid)
        logger.info(" Test suite status is %s", self.tsr["status"])


        self.still_working = True
        while self.still_working:
            # get the list of test cases in the test suite
            self.run_status = self.da_client.get_suite_run(
                suiteDefinitionId=self.tsid, suiteRunId=self.tsrid)
            self.test_cases = self.run_status['testResult']['groups'][0]['tests']

            # build the test case dictionary containing Name:Status
            test_case_dict = {}
            for t in self.test_cases:
                test_case_dict[t["testCaseDefinitionName"]] = t['status']
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    self.still_working = False
                else:
                    self.still_working = True
            # run each test case in test suite
            for t in self.test_cases:
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    s = t['testCaseDefinitionName']
                elif t['status'] == "RUNNING":
                    s = t['testCaseDefinitionName']
                    logger.info('%s%s', '{0:.<60}'.format(s), t['status'])
                    logger.info("starting the pubsub test client ....")
                    test_mqtt = MQTT()
                    test_mqtt.endpoint = self.endpoint()
                    test_mqtt.connect_to_cloud()
                    test_mqtt.mqtt_subscribe(topic="armcsp/sub")
                    test_mqtt.mqtt_publish(
                        topic="armcsp/pub", message="payload")
                else:
                    break
                    logger.warning("unknown test case status! --> %s", t['status'])
                break
            time.sleep(1)
        logger.info("testing completed")

    def run_p_suite(self):
        """Run the test suite and monitor its progress and completion."""
        logger.info("start running the suite")
        try:
            self.start_response = self.da_client.start_suite_run(
                suiteDefinitionId=self.tsid,
                suiteRunConfiguration={
                    'primaryDevice': {
                        'thingArn': self.p_thing_arn
                    },
                    'parallelRun': False
                })
            self.tsrid = self.start_response['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)
        except Exception as e:
            # proceed with existing test suite run and update the test _suite_run_id
            logger.info("Test suite is already running")
            # get the existing run id
            self.definition_response = self.da_client.get_suite_definition(
                suiteDefinitionId=self.tsid
            )
            # print definition response
            self.response = self.da_client.list_suite_runs(
                suiteDefinitionId=self.tsid,
                suiteDefinitionVersion=self.definition_response['suiteDefinitionVersion'])
            self.tsrid = None
            for r in self.response['suiteRunsList']:
                if r['suiteDefinitionId'] == self.tsid and r['status'] == "RUNNING":
                    self.tsrid = r['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)


        self.tsr = self.da_client.get_suite_run(
            suiteDefinitionId=self.tsid,
            suiteRunId=self.tsrid)
        if self.tsr['status'] == 'FAIL' or self.tsr['status'] == 'PASS' or self.tsr['status'] == 'CANCELED':
            pass
        if self.tsr['status'] == 'PENDING':
            logger.info(" waiting for the test suite to be ready for connections...")
            self.interval = 5  # seconds
            sum = 0  # total time waiting
            while self.tsr['status'] == 'PENDING':
                logger.info("%s (%s seconds)", self.tsr['status'], str(sum))
                time.sleep(self.interval)
                sum += self.interval
                self.tsr = self.da_client.get_suite_run(
                    suiteDefinitionId=self.tsid,
                    suiteRunId=self.tsrid)
        logger.info(" Test suite status is %s", self.tsr["status"])


        self.still_working = True
        while self.still_working:
            # get the list of test cases in the test suite
            self.run_status = self.da_client.get_suite_run(
                suiteDefinitionId=self.tsid, suiteRunId=self.tsrid)
            self.test_cases = self.run_status['testResult']['groups'][0]['tests']

            # build the test case dictionary containing Name:Status
            test_case_dict = {}
            for t in self.test_cases:
                test_case_dict[t["testCaseDefinitionName"]] = t['status']
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    self.still_working = False
                else:
                    self.still_working = True
            # run each test case in test suite
            for t in self.test_cases:
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    s = t['testCaseDefinitionName']
                elif t['status'] == "RUNNING":
                    s = t['testCaseDefinitionName']
                    logger.info('%s%s', '{0:.<60}'.format(s), t['status'])
                    logger.info("starting the pubsub test client ....")
                    p_test = Permission()
                    p_test.endpoint = self.endpoint()
                    p_test.connect_to_cloud()
                else:
                    logger.warning("unknown test case status! --> %s", t['status'])
                break
            time.sleep(1)
        logger.info("completed")

    def run_mqtt5_suite(self):
        """Run the MQTT5 test suite and monitor its progress and completion."""
        logger.info("start running the suite")
        try:
            self.start_response = self.da_client.start_suite_run(
                suiteDefinitionId=self.tsid,
                suiteRunConfiguration={
                    'primaryDevice': {
                        'thingArn': self.thing_arn
                    },
                    'parallelRun': False
                })
            self.tsrid = self.start_response['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)

        except Exception as e:
            # proceed with existing test suite run and update the test _suite_run_id
            logger.info("Test suite is already running")
            # get the existing run id
            self.definition_response = self.da_client.get_suite_definition(
                suiteDefinitionId=self.tsid
            )
            # print definition response
            self.response = self.da_client.list_suite_runs(
                suiteDefinitionId=self.tsid,
                suiteDefinitionVersion=self.definition_response['suiteDefinitionVersion'])
            self.tsrid = None
            for r in self.response['suiteRunsList']:
                if r['suiteDefinitionId'] == self.tsid and r['status'] == "RUNNING":
                    self.tsrid = r['suiteRunId']
            logger.info("Test suite run Id = %s", self.tsrid)

        self.tsr = self.da_client.get_suite_run(
            suiteDefinitionId=self.tsid,
            suiteRunId=self.tsrid)
        if self.tsr['status'] == 'FAIL' or self.tsr['status'] == 'PASS' or self.tsr['status'] == 'CANCELED':
            pass
        if self.tsr['status'] == 'PENDING':
            logger.info("waiting for the test suite to be ready for connections...")
            self.interval = 5  # seconds
            sum = 0  # total time waiting
            while self.tsr['status'] == 'PENDING':
                logger.info(f"{self.tsr['status']} ({sum} seconds)")
                time.sleep(self.interval)
                sum += self.interval
                self.tsr = self.da_client.get_suite_run(
                    suiteDefinitionId=self.tsid,
                    suiteRunId=self.tsrid)
        logger.info("Test suite status is " + self.tsr["status"])

        self.still_working = True
        while self.still_working:
            # get the list of test cases in the test suite
            self.run_status = self.da_client.get_suite_run(
                suiteDefinitionId=self.tsid, suiteRunId=self.tsrid)
            self.test_cases = self.run_status['testResult']['groups'][0]['tests']

            # build the test case dictionary containing Name:Status
            test_case_dict = {}
            for t in self.test_cases:
                test_case_dict[t["testCaseDefinitionName"]] = t['status']
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    still_working = False
                else:
                    still_working = True
            # run each test case in test suite
            for t in self.test_cases:
                if t['status'] == 'PASS' or t['status'] == 'PASS_WITH_WARNINGS' or t['status'] == 'ERROR' or t['status'] == "CANCELED" or t['status'] == "STOPPED" or t['status'] == "FAIL":
                    s = t['testCaseDefinitionName']
                    logger.info('%s%s', '{0:.<60}'.format(s), t['status'])
                elif t['status'] == "RUNNING":
                    s = t['testCaseDefinitionName']
                    logger.info('%s%s', '{0:.<60}'.format(s), t['status'])
                    logger.info("starting the pubsub test client ....")
                    test_mqtt = MQTT_5()
                    test_mqtt.endpoint = self.endpoint()
                    test_mqtt.connect_to_cloud()
                    test_mqtt.mqtt_subscribe(topic="armcsp/sub")
                    test_mqtt.mqtt_publish(
                        topic="armcsp/pub", message="payload")
                else:
                    logger.warning("unknown test case status! --> %s", t['status'])
                break
            time.sleep(1)
        logger.info("completed")
