import threading
import json
import time
from awscrt import io, mqtt, auth, http
from src.mqtt_builder import MQTT_builder, on_connection_interrupted, on_connection_resumed, on_resubscribe_complete
from src.logger import get_logger


# Create logger for various debugging logs.
logger = get_logger()

received_count = 0
received_all_event = threading.Event()


class Shadow:
    def __init__(self):
        """Initialize an MQTT client and load AWS IoT configuration for shadow communication."""
        self.mqtt_builder = MQTT_builder(
            "Basic Connect - Make a MQTT connection.")
        self.load_cloud_config()
        self.endpoint = None
        self.cert = self.advisor_config["certificatePath"]
        self.privatekey = self.advisor_config["privateKeyPath"]
        self.rootCA = self.advisor_config["rootCAPath"]

    def load_cloud_config(self):
        """Load AWS IoT configuration from a JSON file."""
        config_file = open("config/advisor_credential.json")
        self.advisor_config = json.load(config_file)

    def connect_to_cloud(self):
        """Establish a connection to the AWS IoT MQTT broker for shadow communication."""
        clientId = "dev_adv"
        port = 8883
        self.conn_handler = self.mqtt_builder.build_mqtt_connection(on_connection_interrupted,
                                                                    on_connection_resumed,
                                                                    self.endpoint, port, self.cert,
                                                                    self.privatekey, self.rootCA, clientId)

        logger.info("Connecting to endpoint with client ID")
        try:
            connect_future = self.conn_handler.connect()
            connect_future.result()
        except:
            time.sleep(10)
            try:
                connect_future = self.conn_handler.connect()
                connect_future.result()
            except:
                time.sleep(10)

        logger.info("Connected to AWS IOT as MQTT client!")

    def reboot_callback(self, topic, payload, dup, qos, retain, **kwargs):
        """Callback function for handling shadow update messages."""
        payload_type = type(payload.decode("utf-8"))
        logger.info("Payload type: %s", payload_type)
        command = str(payload.decode("utf-8")
                      ).strip("\n{}][").replace(" ", "").split(":")
        logger.info("Received command: %s", command)
        global received_count
        received_count += 1
        if received_count == self.advisor_config["count"]:
            received_all_event.set()

    def shadow_subscribe(self, qos=mqtt.QoS.AT_LEAST_ONCE):
        """Subscribe to shadow update messages from AWS IoT."""
        topic = "$aws/things/Device_advisor_thing/shadow/update"
        subscribe_future, packet_id = self.conn_handler.subscribe(
            topic=topic, qos=qos, callback=self.reboot_callback)
        subscribe_result = subscribe_future.result()

    def shadow_publish(self, qos=mqtt.QoS.AT_LEAST_ONCE):
        """Publish a shadow update message to AWS IoT."""
        topic = "$aws/things/Device_advisor_thing/shadow/update"
        message = {"state": {"reported": {"online": "true"},
                             "desired": {"online": "true"}}}
        logger.info("Publishing message to topic '%s': %s", topic, message)
        message_json = json.dumps(message)
        self.conn_handler.publish(
            topic=topic, payload=message_json, qos=qos, retain=True)
