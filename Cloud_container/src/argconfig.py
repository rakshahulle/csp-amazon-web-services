def ArgConf(ap):
    ap.add_argument("-p", "--cloud_port", default="8081",
                    help="Cloud container Flask server port number")
    ap.add_argument("-cw_i", "--cloudwatch_post_interval", default=60,
                    help="Time interval for cloudwatch message posting")
    ap.add_argument("-adv", "--advisor", default="",
                    help="Enable AWS IoT Advisor testing")
    ap.add_argument("-ts", "--DA_testsuite",
                    help="Choose any testsuite like aws qualification/Long duration/custom/MQTT 5")
    ap.add_argument("-rek","--rekognition",help="Amazon Rekognition on cloud")
    args = vars(ap.parse_args())
    return args
