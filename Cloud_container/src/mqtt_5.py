import threading
import json
import time
from awscrt import io, mqtt5, auth, http
from src.mqtt5_builder import MQTT5_builder, on_publish_received, on_lifecycle_connection_success, on_lifecycle_connection_failure, on_lifecycle_stopped, exponential_backoff, backoff_jitter, jitter_backoff
from concurrent.futures import Future
from awsiot import mqtt_connection_builder, mqtt5_client_builder
from src.logger import get_logger

received_count = 0
received_all_event = threading.Event()
future_stopped = Future()
backoff = Future()
future_connection_success = Future()
TIMEOUT = 17500

# Create logger for various debugging logs.
logger = get_logger()

class MQTT_5:
    def __init__(self):
        """Initialize an MQTTv5 client and load AWS IoT configuration."""
        self.mqtt5_client = MQTT5_builder(
            "Basic Connect - Make a MQTT connection.")
        self.load_cloud_config()
        self.endpoint = None
        self.cert = self.advisor_config["certificatePath"]
        self.privatekey = self.advisor_config["privateKeyPath"]
        self.rootCA = self.advisor_config["rootCAPath"]

    def load_cloud_config(self):
        """Load AWS IoT configuration from a JSON file."""
        config_file = open("config/advisor_credential.json")
        self.advisor_config = json.load(config_file)

    def connect_to_cloud(self):
        """Establish a connection to the AWS IoT MQTT broker using MQTTv5 protocol."""
        clientId = "device_csp"
        port = 8883
        self.client = self.mqtt5_client.build_mqtt5_client(endpoint=self.endpoint,
                                                           on_publish_received=on_publish_received,
                                                           on_lifecycle_stopped=on_lifecycle_stopped,
                                                           on_lifecycle_connection_success=on_lifecycle_connection_success,
                                                           on_lifecycle_connection_failure=on_lifecycle_connection_failure,
                                                           backoff_jitter=backoff_jitter,
                                                           jitter_backoff=jitter_backoff,
                                                           exponential_backoff=exponential_backoff)
        logger.info("MQTT5 Client Created")
        self.client.start()
        lifecycle_connect_success_data = future_connection_success.result(
            TIMEOUT)
        sleep = exponential_backoff(exp_backoff)
        wait = jitter_backoff(jitter)
        connack_packet = lifecycle_connect_success_data.connack_packet
        negotiated_settings = lifecycle_connect_success_data.negotiated_settings

    def mqtt_subscribe(self, topic):
        """Subscribe to an MQTT topic using MQTTv5 protocol."""
        logger.info("Subscribing to topic: '%s'", message_topic)
        subscribe_future = client.subscribe(subscribe_packet=mqtt5.SubscribePacket(
            subscriptions=[mqtt5.Subscription(
                topic_filter=message_topic,
                qos=mqtt5.QoS.AT_LEAST_ONCE)]))
        suback = subscribe_future.result(TIMEOUT)
        logger.info("Subscribed with reason codes: %s", suback.reason_codes)

    def mqtt_publish(self, topic, message):
        """Publish an MQTT message to a specified topic using MQTTv5 protocol."""
        logger.info("Publishing message to topic: '%s': %s", topic, message)
        publish_future = client.publish(mqtt5.PublishPacket(
            topic=message_topic,
            payload=message_string,
            qos=mqtt5.QoS.AT_LEAST_ONCE
        ))
        publish_completion_data = publish_future.result(TIMEOUT)
        logger.info("PubAck received with reason code: %s", repr(publish_completion_data.puback.reason_code))
        time.sleep(1)
